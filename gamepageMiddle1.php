<!-- ROLL INFORMATION -->
<div class="center" id="middle-col">
	<div id="wilds">
		

	<!-- GAME STATS -->
	<table class="center" id="statTable">
	<tr><b><th>Player</th><th>Folds Taken</th><th>Folds Given</th><th>Executions</th><th>Five Sixes</th><th>Special Rolls</th></b></tr>
		<?php
		
		while ($statsdata = mysqli_fetch_array($statsdataresult, MYSQLI_ASSOC))
		{
			echo "<tr style='text-align:right;'><td>".$statsdata['username']."</td><td";
			if ($statsdata['folds_taken'] == 3) {
				echo " style='color:red; font-weight:bold;'";
			} 
			echo ">".$statsdata['folds_taken']."</td><td>".$statsdata['folds_given']."</td><td>".$statsdata['executions']."</td><td>".$statsdata['five_sixes']."</td><td></td></tr>";
		}
		?>
	</table>


<?php
// BEGIN FORM AND PASS MAIN VARIABLES


		
	//IF IT IS THE USERS TURN AND STATUS IS FRESH ROLL, DISPLAY ROLL BUTTON, SET STATUS TO fresh
	if ($myTurn == true) {
		// echo $turninfocount."- Turn info count<br />";
		if (/*$turninfocount == 1 and*/ $status == 'fresh')

		{
			echo "
			<form method='post' action='turnprocess.php'>


			<input type='hidden' name='turnID' value='".$turnID."' />
			<input type='hidden' name='turn' value='".$turn."' />
			<input type='hidden' name='gameID' value='".$gameID."' />
			<input type='hidden' name='playerID' value='".$_SESSION['playerID']."' />
			<div class='center'>
					<input type='submit' value='Roll'>
					<input type='hidden' name='wilds' value='0' />
					<input type='hidden' name='proc' value='fresh' />
			</div>
			</form>";
		}
		// OTHERWISE, GENERATE ROLL CUP, RE-ROLL BUTTON, PASS BUTTON, LOOK BUTTON, AND ROLL SELECTS
		else
		{
			if ($status == 'looked'){
				echo "
				<div class='center'>
					<div id='dicecup'>
						<div id='dice'>";
					// echo "$rollArray is looking at the current turn.. should look at previous turn unless it's a reroll";
					
					foreach ($rollArray as $value) {
						echo "[";
							if ($value == 1) {
								echo "<span style='color:#00D10E'>".$value."</span>";
							}
							else {
								echo $value;
							}
						
						echo "]  ";
					}
				echo "</div></div>";
			}
			// print_r($rollArray);
				
			
			if ($status != 'offFresh' and $status != 'rerolled' and $status != 'looked') {
				echo "<form method='post' action='turnprocess.php' id='rerollform'>


						<input type='hidden' name='turnID' value='".$turnID."' />
						<input type='hidden' name='turn' value='".$turn."' />
						<input type='hidden' name='gameID' value='".$gameID."' />
						<input type='hidden' name='playerID' value='".$_SESSION['playerID']."' />

						<!--<div id='rerollbtn' class='center' style='display:none'>
								<input type='submit' value='Re-Roll' >
						</div>-->
								
						<input type='hidden' name='wilds' value=".$wilds." />
						<input type='hidden' name='proc' value='reroll' />
						<input type='hidden' name='playercount' value='".$playercount."' />
					</form>

					";
					
					
			}
			if ($status != 'looked') {
				echo "<form method='post' action='turnprocess.php'>
							<input type='submit' class='bigbutton pullButton' id='pull' value='Pull' />
							<input type='hidden' name='quantitybest' id='quantity' value='".$bestRoll['quantity']."' />
							<input type='hidden' name='valuebest' id='value' value='".$bestRoll['value']."' />
							<input type='hidden' name='quantpassed' id='quantpassed' value='".$quantPassed."' />
							<input type='hidden' name='valuepassed' id='valuepassed' value='".$valuePassed."' />
							<input type='hidden' name='playerID' value='".$_SESSION['playerID']."' />
							<input type='hidden' name='passer' value='".$passer."' />
							<input type='hidden' name='gameID' value='".$gameID."' />
							<input type='hidden' name='turnID' value='".$turnID."' />
							<input type='hidden' name='proc' value='pull' />




						</form>";
			}

			// IF STATUS IS FRESH, OR TURN IS OFF FRESH ROLL, I.E. THERE IS NO CURRENT ROLL TO BEAT, DISPLAY ALL OPTIONS
			
				
					
					if ($status == 'fresh'/* or $status == 'offFresh' or $status == 'looked'*/) { 
						echo 
							"<div class='center'>
								<select name='quantity'>
									<option value='2'>two</option>
									<option value='3'>three</option>
									<option value='4'>four</option>
									<option value='5'>five</option>
								</select>
								<select name='value'>
									<option value='2'>twos</option>
									<option value='3'>threes</option>
									<option value='4'>fours</option>
									<option value='5'>fives</option>
									<option value='6'>sixes</option>
								</select>&nbsp;&nbsp;&nbsp;&nbsp;
							</div>";
					}
					else {
						include "rollselects.php";
					}

						
				
				echo "</form>

				<!-- REROLL FORM -->
				<form method='post' action='turnprocess.php'>
					<input type='submit' class='bigButton rerollButton' value='Re-Roll'>
					<input type='hidden' name='turnID' value='".$turnID."' />
					<input type='hidden' name='turn' value='".$turn."' />
					<input type='hidden' name='gameID' value='".$gameID."' />
					<input type='hidden' name='playerID' value='".$_SESSION['playerID']."' />
					<input type='hidden' name='playercount' value='".$playercount."' />
					<input type='hidden' name='wilds' value='".$wilds."' />
					<input type='hidden' name='proc' value='reroll' />
				</form>";

			
				// echo "<form method='post' action='turnprocess.php'>


				// <input type='hidden' name='turnID' value='".$turnID."' />
				// <input type='hidden' name='turn' value='".$turn."' />
				// <input type='hidden' name='gameID' value='".$gameID."' />
				// <input type='hidden' name='playerID' value='".$_SESSION['playerID']."' />";

				// include "rollselects.php";

				// echo 	"<input type='hidden' name='playercount' value='".$playercount."' />
				// 	<input type='hidden' name='wilds' value='".$wilds."' />
				// 	<input type='hidden' name='proc' value='pass' />



					
				// 	<div class='center'>	
				// 			<input type='submit' class='bigButton passButton' value='Pass'>
				// 	</div>";

				echo "

				<!-- PASS FORM -->
				<form method='post' action='turnprocess.php'>
				
				
					<input type='submit' class='bigButton passButton' value='Pass'>

					<input type='hidden' name='turnID' value='".$turnID."' />
					<input type='hidden' name='turn' value='".$turn."' />
					<input type='hidden' name='gameID' value='".$gameID."' />
					<input type='hidden' name='playerID' value='".$_SESSION['playerID']."' />
					<input type='hidden' name='playercount' value='".$playercount."' />
					<input type='hidden' name='wilds' value='".$wilds."' />
					<input type='hidden' name='proc' value='pass' />";

				if ($status != 'looked' || $status != 'rerolled') {
					echo "<input type='button' class='bigButton lookButton' id='look' value='Look' >";
				}
			
				/*if ($status == 'rolled' or $status == 'offFresh' or $status == 'unrolled') {
					echo "<input type='button' id='look' value='Look' >";
				}*/
				
			echo "</div>
				</form>";
		}
	}

?>

</div>