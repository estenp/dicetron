<?php
session_start();
$userID = $_SESSION['userID'];
$gameID = $_GET['gameID'];
include "includes.php";
// include "dbconnect.php";

echo "<div id='ninesixty'>";

include "inc/header.inc.php";

$player = new player;
$game = new game;
$Stats = new stats;
$turn = new turn;

$gameInfo = $game->getGameInfo($gameID, $dbh);
$playerInfo = $player->getPlayerInfo($userID, $gameID, $dbh);
$allPlayersInfo = $player->getAllPlayersInfo($gameID, $dbh);
$getGameStats = $Stats->getGameStats($gameID, $dbh);

// print_r($allPlayersInfo);
// exit();


// PLAYER CHECK
if ($playerInfo)
{
	echo "<div data-role='navbar'><ul><li><a href='games.php'>Back</a></li>
	<li><a href='gamepage.php?gameID=".$gameID."'>Go to Game</a></li></ul></div>";
	//if player is the game leader, allow to activate the game
	if ($gameInfo['game_leader'] == $playerInfo['playerID'] && $gameInfo['status'] == 0) {
		echo "<input id='activate' type='button' value='Activate Game' onClick='activateGame(".$gameID.")' />";
	}
} else {
	if ($gameInfo['password']) {
		// echo '<input type="button" onClick="joinGame(';
		// echo $gameID;
		// echo ')" value="Join Game" />';
		echo '<input type="text" id="password" placeholder="Password"><input type="button" value="Join Game" id="joinBTN">';
	}
	else {
		$url = "joinprocess.php?gameID=".$gameID;
		echo '<input type="button" onClick="window.location.replace(\''.$url.'\')" value="Join Game" />';
	}

}

?>

<h3><?php echo $gameInfo['name']; ?></h3>

<div class="column">

	<div id="gameinfo">
		<table>
			<tr><td>Player Count: </td><td><?php echo $game->getPlayerCount($gameID, $dbh)." of ".$gameInfo['playermax']; ?></td></tr>
			<tr><td>Status: </td><td id='status'><?php if ($gameInfo['status'] == 0){echo "Inactive";}else{echo "Active";}?></td></tr>
			<tr><td>Current Turn: </td><td><?php if ($gameInfo['status'] !== 0)
			{
				$turnuser = $turn->getusernameforcurrentturn($gameInfo['turn'], $gameID, $dbh);
				echo $turnuser; }
			else {
				echo "Inactive";
			}?></td></tr>
		</table>
	</div>
	<!-- <div id="playerlist">
		<table>
			<tr><th>Turn Position</th><th>Player</th></tr>
			<?php
	  		foreach ($allPlayersInfo as $player) {
				echo "<tr><td>".$player['turn_position']."</td><td>".$player['username']."</td></tr>";
	  		}
	  		?>
		</table>
	</div> -->
</div>

<div class="column">
	<table class="center" id="statTable" >
	<tr><b><th>Player</th><th>Turn Position</th><th>Folds Taken</th><th>Folds Given</th></b></tr>
		<?php

		foreach ($getGameStats as $statsdata)
		{
			echo "<tr style='text-align:right;";
			if ($statsdata['folds_taken'] == 4) {
				echo " color:#aaa; font-style:italic;'";
			}
			echo "'><td>".$statsdata['username']."</td><td>".$statsdata['turn_position']."</td><td";
			if ($statsdata['folds_taken'] == 3) {
				echo " style='color:red; font-weight:bold;'";
			}

			echo ">".$statsdata['folds_taken']."</td><td>".$statsdata['folds_given']."</td><td></td></tr>";
		}
		?>
	</table>
</div>

<?php include_once "inc/footer.inc.php"; ?>
<div class="clear"></div>
</div> <!-- end ninesixty -->

<script>
$(document).on('pageinit', function(){
	var gameID = <?php echo $gameID; ?>;
	$('#joinBTN').click(function(){

		$.post('ajax/checkGamePass.ajax.php', {pass:$('#password').val(), gameID:gameID}, function(response){
			if (response == 1) {
				// alert('yes');
				//close dialog
				// $this.dialog( "close" );
				//redirect to join process
				window.location.replace("joinprocess.php?gameID="+gameID);
			}
			else {
				$('#password').val('').css('background-color','#FF9090');
			}
		});
	});
});
// function joinGame(gameID){
//   //IF GAME HAS A PASSWORD, POP UP A DIALOG TO ENTER IT
//   var pass = password;
//   // alert(pass);
//   // alert(pass);
//    $( "#passdiv" ).dialog({
//     height: 250,
//     width: 500,
//     modal: true,
//     title: "Password Required",
//     buttons: {
// 	    Cancel: function() {
// 	    	$(this).dialog( "close" );
// 	    },
// 	    "Submit Password": function() {
// 	    	var $this = $(this);
// 	    	$.post('ajax/checkGamePass.ajax.php', {pass:$('#password').val(), gameID:gameID}, function(response){
// 	    		if (response == 1) {
// 	    			// alert('yes');
// 	    			//close dialog
// 	    			$this.dialog( "close" );
// 	    			//redirect to join process
// 	    			window.location.replace("joinprocess.php?gameID="+gameID);
// 	    		}
// 	    		else {
// 	    			$('#password').val('').css('background-color','#FF9090');
// 	    		}
// 	    	});


// 	    	// if ($('#password').val() === password){
// 	    	// 	//close dialog
// 	    	// 	$(this).dialog( "close" );
// 	    	// 	//redirect to join process
// 	    	// }
// 	    	// else {
// 	    	// 	$('#password').val('').css('background-color','red');
// 	    	// }
// 	    }
// 	}
//   	});
// }



//$(document).ready(function(){

function activateGame(gameID){
	$.post("ajax/activateGame.ajax.php", {gameID:gameID}, function(){
		$('#activate').hide();
		$('#status').html('Active');
	});
}

//});

</script>

<div id='passdiv' style='display:none;'>
	<input type='text' name="password" id='password' required>

</div>
