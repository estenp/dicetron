<?php



//PLAYER INFO
//gather player information
$Player = new player;
$getPlayerInfo = $Player->getPlayerInfo ($userID, $gameID, $dbh);
$getAllPlayersInfoCount = $Player->getAllPlayersInfoCount ($gameID, $dbh);

//PLAYER COUNT
//gather player count
$Game = new game($gameID);
$playercount = $Game->getPlayerCount ($gameID, $dbh);
//GAME INFO
//gather game information
$gameinfo = $Game->getGameInfo ($gameID, $dbh);

//TURN INFO
//gather turn count and other turn info
$Turn = new turn;
$turninfo = $Turn->getTurnInfo ($gameID, $dbh);


//PREVIOUS TURN INFO
//gather turn count and other turn info

$prevturninfo = $Turn->getPreviousTurnInfo ($gameID, $dbh);


// SELECT MOST RECENT ROLL
$Roll = new roll;


//get game stats
$Stats = new stats;
$getGameStats = $Stats->getGameStats($gameID, $dbh);


//get timeline
$timeline = new timeline;
$getTimeline = $timeline->getTimeline($gameID, $dbh);

//get chat
$Chat = new chat;
$getChat = $Chat->getChat($gameID, $dbh);

?>
