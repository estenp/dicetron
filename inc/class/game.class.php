<?php

class game {

	public $gameID;


	// public function __construct($gameID)
	// {
	// 	$this->gameID = $gameID;
	// }

	public function activateGame ($gameID, $dbh)
	{
		$stmt = $dbh->prepare("update games set status = 1, start_date = CURRENT_TIMESTAMP where gameID = :gameID");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
	}

	public function addGame ($name, $password, $playermax, $dbh) {
		$stmt = $dbh->prepare("insert into games (name, playermax, password) values (:name, :playermax, :password)");
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':playermax', $playermax);
		$stmt->bindParam(':password', $password);
		$stmt->execute();
		$gameID = $dbh->lastInsertID();
		return $gameID;

	}


	public function getGameInfo ($gameID, $dbh)
	{
		$stmt = $dbh->prepare("select * from games where gameID = :gameID");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);


		return $result;
	}

	public function getPlayerCount ($gameID, $dbh)
	{

		$stmt = $dbh->prepare("select * from players p join games g on (g.gameID = p.gameID) where g.gameID = :gameID");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->rowcount();

		return $result;
	}

	// public function joinGame ()
	// {

	// }

	public function updateGameStatus ($gameID, $status, $dbh)
	{
		$stmt = $dbh->prepare("update games set status = :status where gameID = :gameID");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->bindParam(':status', $status);
		$stmt->execute();
	}


	public function updateGameLeader ($gameID, $playerID, $dbh)
	{
		$stmt = $dbh->prepare("update games set game_leader = :playerID where gameID = :gameID");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->bindParam(':playerID', $playerID);
		$stmt->execute();
	}


	public function updateGameToTurn($turn, $gameID, $dbh)
	{
		$stmt = $dbh->prepare("update games set turn = :turn where gameID = :gameID");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->bindParam(':turn', $turn);
		$stmt->execute();

	}


	public function winGame ($gameID, $playerID, $dbh)
	{
		$stmt = $dbh->prepare("update games set winner = :playerID, end_date = now() where gameID = :gameID");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->bindParam(':playerID', $playerID);
		$stmt->execute();
	}


}

?>
