<?php

class chat {
	
	public $gameID;


	public function addToChat ($gameID, $playerID, $message, $dbh)
	{
		$stmt = $dbh->prepare("INSERT INTO chat (gameID, playerID, message) VALUES (:gameID, :playerID, :message)");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->bindParam(':playerID', $playerID);
		$stmt->bindParam(':message', $message);
		$stmt->execute();
	}

	
	public function getChat ($gameID, $dbh)
	{
		$stmt = $dbh->prepare("SELECT * FROM chat WHERE gameID = :gameID ORDER BY add_date");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;

	}
}

?>