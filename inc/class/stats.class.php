<?php

class stats {

	public function addAce($playerID, $dbh)
	{
		$stmt = $dbh->prepare("update stats set aces = aces + 1 where playerID = :playerID");
		$stmt->execute(array(':playerID' => $playerID));
	}


	public function addExecution ($playerID, $dbh)
	{
		$stmt = $dbh->prepare("update stats set executions = executions + 1 where playerID = :playerID");
		$stmt->execute(array(':playerID' => $playerID));
	}


	public function addFold ($taker, $giver, $dbh)
	{

		$stmt = $dbh->prepare("update stats set folds_taken = folds_taken + 1 where playerID = :playerID");
		$stmt->execute(array(':playerID' => $taker));

		$stmt = $dbh->prepare("update stats set folds_given = folds_given + 1 where playerID = :playerID");
		$stmt->execute(array(':playerID' => $giver));
	}


	public function addFiveSixes($playerID, $dbh)
	{
		$stmt = $dbh->prepare("update stats set five_sixes = five_sixes + 1 where playerID = :playerID");
		$stmt->execute(array(':playerID' => $playerID));
	}


	public function addNatural($playerID, $dbh)
	{
		$stmt = $dbh->prepare("update stats set naturals = naturals + 1 where playerID = :playerID");
		$stmt->execute(array(':playerID' => $playerID));
	}



	public function addStatRecord ($playerID, $gameID, $dbh)
	{
		$stmt = $dbh->prepare("insert into stats (playerID, gameID) values (:playerID, :gameID)");
		$stmt->bindParam(':playerID', $playerID);
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$statID = $dbh->lastInsertID();
		return $statID;
	}


	public function getGameStats ($gameID, $dbh) {

		$stmt = $dbh->prepare("
		select s.*, u.username, p.turn_position
		from stats s
		join players p on  s.playerID = p.playerID
		join users u on p.userID = u.userID
		where s.gameID = :gameID order by p.turn_position");
		///*order by s.folds_taken asc*/
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}


	public function getPlayerStats ($playerID, $gameID, $dbh)
	{
		$stmt = $dbh->prepare
		("SELECT * FROM stats WHERE playerID = :playerID AND gameID = :gameID");
		$stmt->execute(array(':playerID' => $playerID, ':gameID' => $gameID));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}


	public function playerOut ($taker, $gameID, $turn, $playercount, $dbh)
	{
		$updatePlayerStatus = new player;
		$updatePlayerStatus->updatePlayerStatus ('0', $taker, $dbh);

		// $advanceTurn = new turn;
		// $newTurn = $advanceTurn->advanceTurn($turn, $gameID, $playercount, $dbh);
		//
		//
		// $updateGameToTurn = new game;
		// $updateGameToTurn->updateGameToTurn($newTurn, $gameID, $dbh);

		// was causing two execution - happens in turnprocessing
		// $this->addExecution($giver, $dbh);
	}


}

?>
