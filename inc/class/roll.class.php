<?php

class roll extends turn {


	//PULLS LAST ROLL FROM DATABASE FOR A GIVEN GAMEID
	//OUPUTS AN ARRAY CONTAINING ROLL VALUES I.E. (1, 2, 2, 4, 6)
	public function getLastRoll ($gameID, $dbh)
	{

		$stmt = $dbh->prepare("select * from rolls where gameID = :gameID order by roll_time desc limit 0, 1");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$rollArray = Array();
		$wilds = 0;
		foreach ($result as $fetchRoll) {
			// echo $fetchRoll['fours'];

			for ($i = 0; $i < $fetchRoll['wilds']; $i++) {
				$rollArray[] = 1;
				$wilds = ++$wilds;

			}
			for ($i = 0; $i < $fetchRoll['twos']; $i++) {
				$rollArray[] = 2;
			}
			for ($i = 0; $i < $fetchRoll['threes']; $i++) {
				$rollArray[] = 3;
			}
			for ($i = 0; $i < $fetchRoll['fours']; $i++) {
				$rollArray[] = 4;
			}
			for ($i = 0; $i < $fetchRoll['fives']; $i++) {
				$rollArray[] = 5;
			}
			for ($i = 0; $i < $fetchRoll['sixes']; $i++) {
				$rollArray[] = 6;
			}
		}
		return $rollArray;
	}



	public function insertRoll ($quantCounts, $playerID, $turnID, $gameID, $dbh)
	{

		$stmt = $dbh->prepare("insert into rolls 
		(playerID, turnID, gameID, wilds, twos, threes, fours, fives, sixes) 
		values 
		(:playerID,:turnID,:gameID, :wilds, :twos, :threes, :fours, :fives, :sixes)");
		$stmt->execute(array(':playerID' => $playerID, ':turnID' => $turnID, ':gameID' => $gameID, ':wilds' => $quantCounts[1], ':twos' => $quantCounts[2], ':threes' => $quantCounts[3], ':fours' => $quantCounts[4], ':fives' => $quantCounts[5], ':sixes' => $quantCounts[6]));
	}



	//TAKES IN A QUANTITY COUNT ARRAY
	//OUTPUTS A TWO KEY ARRAY OF THE BEST POSSIBLE SCORE FOR THAT ROLL I.E. KEY 'VALUE', 'QUANTITY' VALUES: 4, 5
	public function judgeRoll ($quantCounts) 
	{

		//set variables for highest die and highest quantity
		$maxQuant = 0;
		$maxValue = 0;
		//determine highest roll possible
		//loop through dice count excluding wilds (1) and set highest count as max
		for ($i = 2; $i < 7; $i++) {
			$dicecount = $quantCounts[$i];
			if ($dicecount > $maxQuant) {
				$maxQuant = $dicecount;
				$maxValue = $i;
			}
			else if ($dicecount == $maxQuant) {
				if ($i > $maxValue) {
					$maxValue = $i;
				}
			}
		}
		//add final max to the quant of wilds to get the highest roll
		$maxQuant = $maxQuant + $quantCounts[1];
		$bestroll = array('value'=>$maxValue, 'quantity'=>$maxQuant);
			
		return $bestroll;

	}


	//GENERATES A NEW ROLL
	//OUTPUTS AN ARRAY OF THE COUNT OF DICE FOR EACH VALUE IN THE ROLL. I.E. KEYS: 1 2 3 4 5 6 AND VALUES: 0 3 0 1 2 
	public function newroll ($wilds) 
	{
		$quantity = 5 - $wilds;
		$rollArray = array();
		for ($i = 1; $i <= $quantity; $i++)
		{
			$rollArray[] = rand(1, 6);
		}
		$quantCounts = $this->valueArraytoQuantArray($rollArray);


		return $quantCounts;
	}


	//TAKES A ROLL VALUE ARRAY
	//OUTPUTS AN ARRAY OF THE COUNT OF DICE FOR EACH VALUE IN THE ROLL. I.E. KEYS: 1 2 3 4 5 6 AND VALUES: 0 3 0 1 2 
	public function valueArraytoQuantArray ($rollArray) 
	{
		//create an array containing a counter for each value (1-6) with keys (for easier reference)
		$quantCounts = array(1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0);

		//sort the roll and place the quantities for each given value in their respected array
		foreach ($rollArray as $value) {

			//"counts" the quantity of each value in the roll
			if ($value == 1) {
				$quantCounts[1] == $quantCounts[1]++;
			}
			else if ($value == 2) {
				$quantCounts[2] == $quantCounts[2]++;
			}
			else if ($value == 3) {
				$quantCounts[3] == $quantCounts[3]++;
			}
			else if ($value == 4) {
				$quantCounts[4] == $quantCounts[4]++;
			}
			else if ($value == 5) {
				$quantCounts[5] == $quantCounts[5]++;
			}
			else if ($value == 6) {
				$quantCounts[6] == $quantCounts[6]++;
			}
			
		}

		return $quantCounts;
	}


	

}

?>