<?php

class player {
	public $userID;
	public $gameID;

	public function addPlayer ($userID, $gameID, $turnpos, $dbh)
	{
		$stmt = $dbh->prepare("insert into players (userID, gameID, turn_position, status) values (:userID, :gameID, :turnpos, 1)");
		$stmt->bindParam(':userID', $userID);
		$stmt->bindParam(':gameID', $gameID);
		$stmt->bindParam(':turnpos', $turnpos);
		$stmt->execute();
		return $dbh->lastInsertID();
	}



	//retrieve player information
	public function getPlayerInfo ($userID, $gameID, $dbh)
	{
		$stmt = $dbh->prepare("
		select p.* from players p
		join games g on (p.gameID = g.gameID)
		join users u on (u.userID = p.userID)
		where u.userID = :userID and g.gameID = :gameID /*and p.status = 0*/");
		$stmt->bindParam(':userID', $userID);
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result;
	}

	//retrieve player information
	public function getPlayerInfoByPlayerID ($playerID, $dbh)
	{
		// echo 'playerID:'.$playerID;
		$stmt = $dbh->prepare("
		select p.* from players p
		where p.playerID = :playerID");
		$stmt->bindParam(':playerID', $playerID);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		// echo 'playerINFO:';
		// print_r($result);
		return $result;
	}


	//retrieve player status by turn position
	public function getPlayerStatusByTurn ($turn, $gameID, $dbh)
	{
		echo 'turn pos:'.$turn;
		echo 'gameID:'.$gameID;
		$stmt = $dbh->prepare("select status from players where turn_position = :turn and gameID = :gameID");
		$stmt->bindParam(':turn', $turn);
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}



	//retrieve player information
	public function getAllPlayersInfo ($gameID, $dbh)
	{
		$stmt = $dbh->prepare("select p.* from players p
		join games g on (p.gameID = g.gameID)
		join users u on (u.userID = p.userID)
		where g.gameID = :gameID and p.status = 1");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}



	//retrieve player information
	public function getAllPlayersInfoCount ($gameID, $dbh)
	{
		$stmt = $dbh->prepare("select * from players p
		join games g on (p.gameID = g.gameID)
		join users u on (u.userID = p.userID)
		where g.gameID = :gameID and p.status = 1");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->rowCount();

		return $result;
	}





	public function getPlayerName ($playerID, $dbh)
	{
		$stmt = $dbh->prepare("
		select u.username from users u
		join players p on p.userID = u.userID
		where p.playerID = :playerID");
		$stmt->bindParam(':playerID', $playerID);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result['username'];

	}





	//checks if the user is already in this game
	public function isPlayer ($userID, $gameID, $dbh)
	{
		$stmt = $dbh->prepare("select * from games g join players p on (p.gameID = g.gameID) where p.userID = :userID and g.gameID = :gameID");
		$stmt->bindParam(':userID', $userID);
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;

	}




	public function updatePlayerStatus ($status, $playerID, $dbh)
	{

		$stmt = $dbh->prepare("update players set status = :status where playerID = :playerID");
		$stmt->bindParam(':status', $status);
		$stmt->bindParam(':playerID', $playerID);
		echo $status;
		echo $status;
		$stmt->execute();
	}




}

?>
