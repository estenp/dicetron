<?php

class timeline {

	public $gameID;


	public function addToTimeline ($gameID, $code, $message, $dbh)
	{
		$stmt = $dbh->prepare("INSERT INTO timeline (gameID, code, message) VALUES (:gameID, :code, :message)");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->bindParam(':code', $code);
		$stmt->bindParam(':message', $message);
		$stmt->execute();
	}


	public function getTimeline ($gameID, $dbh)
	{
		$stmt = $dbh->prepare("SELECT * FROM timeline WHERE gameID = :gameID ORDER BY add_date desc");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;

	}

	public function getLastTimelineEvent ($gameID, $dbh)
	{
		$stmt = $dbh->prepare("SELECT * FROM timeline WHERE gameID = :gameID ORDER BY add_date desc");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;

	}
}

?>
