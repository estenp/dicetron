<?php

class turnprocessing extends turn {




	public function lookProcess ($gameID, $playerID, $turnID, $dbh)
	{
		$timeline = new timeline;
		$player = new player;
		$this->updateTurnStatus ('looked', $turnID, $dbh);
		$looker = $player->getPlayerName($playerID, $dbh);
		$message = $looker." looked at the roll.";
		// echo $message;

		$timeline->addToTimeline($gameID, 4, $message, $dbh);


	}





	public function passProcess ($playerID, $turn, $gameID, $quantity, $value, $wildspulled, $turnID, $playercount, $dbh)
	{
		$this->updateTurnToCurrentRoll($quantity, $value, $dbh);
		//if the passed roll is 5 6's, pull.
		$turn = $this->advanceTurn ($turn, $gameID, $playercount, $dbh);
		//update game record to current turn (Game class)
		$this->updateGameToTurn($turn, $gameID, $dbh);
		//insert new turn, with status 'covered'
		$this->addNextTurn ($playerID, $gameID, $quantity, $value, $wildspulled, 'covered', 0, $dbh);
		// if ($_POST['quantity'] == 5 and $_POST['value'] == 6) {
		// 	echo 'passer:'. $passer;
		// 	echo 'receiver'.$playerID;
		// 	$this->pullProcess($wildspulled, $quantity, $value, $playerID, $passer, $gameID, $turnID, $playercount, $turn, $dbh);
		// }
		// else {
		// }


		$passer = $this->getTurnInfo($gameID, $dbh);
		$passer = $passer['playerID'];

		// $recip = $this->getPreviousTurnInfo($gameID, $dbh);
		// $passer = $previous['playerID'];

		// print_r($current);
		// print_r($previous);
		// echo "<script>alert(";
		// echo $passer;
		// echo ");</script>";

		// print_r($Turn->getTurnInfo($gameID, $dbh));


		$timeline = new timeline;
		$player = new player;
		$game = new game;
		$recip = $this->getUsernameForCurrentTurn($turn, $gameID, $dbh);

		$passer = $player->getPlayerName($passer, $dbh);
		// echo $passer;
		// echo $recip;


		$message = $passer." passed ".$quantity." ".$value."'s to ".$recip;
		$timeline->addToTimeline($gameID, 3, $message, $dbh);
	}





	function pullProcess ($wildspulled, $qpassed, $vpassed, $playerID, $passer, $gameID, $turnID, $playercount, $currentTurn, $dbh) {

		// echo $playerID.' '.$passer;


		$roll = new roll;
		$player = new player;
		$timeline = new timeline;
		$game = new game;
		$stats = new stats;


		//pull last roll
		$lastRoll = $roll->getLastRoll($gameID, $dbh);
		//convert fields to array
		$lastRollQuantArray = $roll->valueArraytoQuantArray ($lastRoll);
		//add previously pulled wilds to the wilds key
		$lastRollQuantArray[1] = $lastRollQuantArray[1] + $wildspulled;

		//judge this roll
		$judge = $roll->judgeRoll($lastRollQuantArray);

		$qbest = $judge['quantity'];
		$vbest = $judge['value'];

		$BR = $qbest.$vbest;
		$PR = $qpassed.$vpassed;

		// echo 'best'.$BR;
		// echo 'passed'.$PR;


		if ($PR <= $BR) {
			$hadit = true;
			$taker = $playerID;
			$giver = $passer;
			echo "had it";

		}
		else {
			$hadit = false;
			$taker = $passer;
			$giver = $playerID;
			echo "didn't have it";
		}

		// echo $taker.' '.$giver;



		//queries for adding stats, other things?
		if ($BR == '56') {
			$stats->addFiveSixes($giver, $dbh);
			if ($lastRollQuantArray[1] == 6) {
				$stats->addAce($giver, $dbh);
			}
			else if ($lastRollQuantArray[6] == 6) {
				$stats->addNatural($giver, $dbh);
			}
		}

		// $gettaken = "select folds_taken from stats where playerID = ".$taker;
		// $getgiven = "select folds_given from stats where playerID = ".


		//add pull to timeline
		$takername = $player->getPlayerName($taker, $dbh);
		$givername = $player->getPlayerName($giver, $dbh);

		$recip = $this->getUsernameForCurrentTurn($currentTurn, $gameID, $dbh);

		$message = $recip." pulled it. ".$takername." has taken a fold.";
		$timeline->addToTimeline($gameID, 6, $message, $dbh);


		//update given and taken
		$stats->addFold($taker, $giver, $dbh);
		$getStats = $stats->getPlayerStats($taker, $gameID, $dbh);

		$foldsCount = $getStats['folds_taken'];

		//if taker has 4 folds, set player status to out (0) and give giver an execution
		if ($foldsCount >= 4) {

			//change the taker's status to inactive
			$player->updatePlayerStatus ('0', $taker, $dbh);


			$playerInfo = $player->getPlayerInfoByPlayerID($taker, $dbh);
			$takerTurnPos = $playerInfo['turn_position'];
				// echo "taker".$taker;
				// echo "currentTurn".$currentTurn;
				// echo "takerTurnPos<br>";
				// print_r($playerInfo);
				// exit;
			//if the puller is taking the fold/execution, the game's turn needs to be advanced.
			if ($takerTurnPos == $currentTurn) {
				$newTurn = $this->advanceTurn($currentTurn, $gameID, $playercount, $dbh);
				$game->updateGameToTurn($newTurn, $gameID, $dbh);
			}

			//award an execution
			$stats->addExecution($giver, $dbh);

			//post execution to the timeline
			$message = $givername." executed ".$takername."!";
			$timeline->addToTimeline($gameID, 7, $message, $dbh);

			//check the active players to see if the game has been won
			$activePlayers = $player->getAllPlayersInfo($gameID, $dbh);
			$activePlayerCount = count($activePlayers);
			// print_r($activePlayers);
			// exit;


			if ($activePlayerCount == 1) {
				// echo "activeplayers:".$activePlayerCount;
				// echo "last player left:".$activePlayers[0]['playerID'];
				// exit;
				//update game winner to the last active playerID
				$game->winGame($gameID, $activePlayers[0]['playerID'], $dbh);
				//add to timeline that the user won
				$message = $givername." won the game!";
				$timeline->addToTimeline($gameID, 7, $message, $dbh);
				//make game inactive, set game end date
				$game->updateGameStatus($gameID, 0, $dbh);
			}




		} else if ($foldsCount == 3) {
			//post an E fold notification to the timeline
			$message = $takername." is on their E fold!";
			$timeline->addToTimeline($gameID, 7, $message, $dbh);
		}

		//add five sixes, naturals, aces

		//set roll status to fresh
		$this->updateTurnStatus('fresh', $turnID, $dbh);
		$this->updateTurnToFresh ($turnID, $dbh);



	}





	public function rollProcess ($playerID, $turnID, $gameID, $wilds, $wildspulled, $proc, $dbh)
	{

		$timeline = new timeline;
		$player = new player;

		//add pull to timeline
		$roller = $player->getPlayerName($playerID, $dbh);


		$roll = new roll;
		if ($proc == 'fresh')
		{
			$this->updateTurnToFresh ($turnID, $dbh);
			$this->updateTurnStatus ('off fresh', $turnID, $dbh);
			$message = $roller." rolled a fresh roll.";
			$timeline->addToTimeline($gameID, 1, $message, $dbh);

		}
		else if ($proc == 'reroll')
		{
			$this->updateTurnStatus ('rerolled', $turnID, $dbh);
			$this->pullWilds($turnID, $wilds, $dbh);
			$wilds = $wilds + $wildspulled;
			$message = $roller." rerolled.";
			$timeline->addToTimeline($gameID, 2, $message, $dbh);

		}


		$quantCounts = $roll->newroll($wilds);
		// $wilds = $wildspulled + $wilds;
		$roll->insertRoll ($quantCounts, $playerID, $turnID, $gameID, $dbh);

	}





}

?>
