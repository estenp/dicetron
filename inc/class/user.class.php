<?php


class user {
	public $userID;


	//retrieve player information
	public function getUserGames ($userID, $dbh)
	{
		$stmt = $dbh->prepare("select p.playerID as playerID, g.gameID as gameID, g.turn as turn, g.name as name, p.turn_position 
	from games g
	join players p on (p.gameID = g.gameID)
	where p.userID = :userID ;");
		$stmt->bindParam(':userID', $userID);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}


	public function getUserID ($username, $dbh)
	{
		$stmt = $dbh->prepare("select userID, username from users where username = :username");
		$stmt->bindParam(':username', $username);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result;

	}


	public function getNonUserGames ($userID, $dbh)
	{
		$stmt = $dbh->prepare("
		select *
		from games g where g.gameID not in (
			select gameID from players p
			join users u on (u.userID = p.userID)
			where u.userID = :userID)
		");
		$stmt->bindParam(':userID', $userID);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}




	//checks if the user is already in this game
	public function isPlayer ($userID, $gameID, $dbh)
	{
		$stmt = $dbh->prepare("select * from games g join players p on (p.gameID = g.gameID) where p.userID = :userID and g.gameID = :gameID");
		$stmt->bindParam(':userID', $userID);
		$stmt->bindParam(':gameID', $gameID);

		$stmt->execute();


		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		// print_r($result);

		if ($result != null)

		{
			return true;
		}
		else
		{
			return false;
		}
	}




	public function updatePlayerStatus ($status, $playerID, $dbh)
	{
		$stmt = "update players set status = :status where playerID = :playerID";
		$stmt->bindParam(':status', $status);
		$stmt->bindParam(':player', $player);
		$stmt->execute();
	}



	public function verifyUsername ($username, $dbh)
	{
		$stmt = $dbh->prepare("select userID from users where username = :username ;");
		$stmt->bindParam(':username', $username);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}




}

?>
