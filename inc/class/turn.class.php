<?php

class turn {



	public $turnID;




	public function addNextTurn ($playerID, $gameID, $quantity, $value, $wildspulled, $status, $fresh, $dbh)
	{

		$stmt = $dbh->prepare("insert into turns (playerID, gameID, q_received, v_received, wilds_pulled, status, fresh) values (:playerID, :gameID, :quantity, :value, :wilds, :status, :fresh)");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->bindParam(':playerID', $playerID);
		$stmt->bindParam(':quantity', $quantity);
		$stmt->bindParam(':value', $value);
		$stmt->bindParam(':wilds', $wildspulled);
		$stmt->bindParam(':status', $status);
		$stmt->bindParam(':fresh', $fresh);

		$stmt->execute();
	}




	public function advanceTurn ($turn, $gameID, $playercount, $dbh)
	{


		$turn = $turn + 1;

		//if the incremented turn is greater than the number of people playing, go back to 1
		if ($turn > $playercount) {

			$turn = 1;
		}
		// echo $turn;
		//if this status is inactive, continue to increment the turn until it lands on an active player
		// $i = 0;
		// while ($i == 0) {
		for ($i = 0; $i < 1;) {
			$player = new player;
			$result = $player->getPlayerStatusByTurn($turn, $gameID, $dbh);
			// print_r($result);

			//if the player on the current turn is out of the game, add 1 to turn to go to the next player
			if ($result['status'] == 0) {

				$turn = $turn + 1;
				if ($turn > $playercount) {

					$turn = 1;
				}
				$i = 0;
			// echo $i;
			// exit;
			}

			else {

				$i = 1;
			}
			// echo $i;
		}

		//send email to current turn
		$getUserIDForCurrentTurn = $this->getUserIDForCurrentTurn($turn, $gameID, $dbh);
		$game = new game;
		$gameInfo = $game->getGameInfo($gameID, $dbh);
		$gameName = $gameInfo['name'];

		$gameURL = "www.estenpatrick.com/gamedetail.php?gameID=".$gameID;

		$subject = "It is your turn in DiceTron!";
		$content = "You're up in game: <a href='".$gameURL."'>".$gameName."</a>. Hurry up!";

		$email = new email;
		$email->sendEmail($getUserIDForCurrentTurn, $subject, $content, $dbh);

		// echo $turn;
		// exit;
		return $turn;

	}





	public function getPreviousTurnInfo ($gameID, $dbh)
	{

		$stmt = $dbh->prepare("
		select * from turns t
		join games g on (t.gameID = g.gameID)
		where g.gameID = :gameID LIMIT 1, 1");
		$stmt->bindParam(':gameID', $gameID);
		// $stmt->bindParam(':turnID', $this->turnID);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}



//this needs to change
	public function getPasserName ($gameID, $dbh)
	{
		$stmt = $dbh->prepare("
		select u.username from turns t
		join players p on p.playerID = t.playerID
		join users u on p.userID = u.userID
		where t.gameID = :gameID
		order by t.turnID DESC LIMIT 1, 1");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result['username'];
	}




	public function getTurnInfo ($gameID, $dbh)
	{
		$stmt = $dbh->prepare("
		select t.*, g.turn from turns t
		join games g on (t.gameID = g.gameID)
		where g.gameID = :gameID
		order by add_date desc
		LIMIT 1");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$this->turnID = $result['turnID'];
		return $result;

	}




	public function getUsernameForCurrentTurn ($turn, $gameID, $dbh)
	{

		$stmt = $dbh->prepare("
		select username from users u
		join players p on (p.userID = u.userID)
		where p.turn_position = :turn and gameID = :gameID");
		$stmt->bindParam(':gameID', $gameID);
		//set in turnInfo?
		$stmt->bindParam(':turn', $turn);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result['username'];
	}

	public function getUserIDForCurrentTurn ($turn, $gameID, $dbh)
	{

		$stmt = $dbh->prepare("
		select u.userID from users u
		join players p on (p.userID = u.userID)
		where p.turn_position = :turn and gameID = :gameID");
		$stmt->bindParam(':gameID', $gameID);
		//set in turnInfo?
		$stmt->bindParam(':turn', $turn);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		// print_r($result['userID']);
		// exit;
		return $result['userID'];
	}



//turn + 1 won't work if they are the last person. do we need on deck or a better view of turn order
	public function getUsernameForOnDeck ($turn, $gameID, $dbh)
	{

		$stmt = $dbh->prepare("
		select username from users u
		join players p on (p.userID = u.userID)
		where p.turn_position = (:turn + 1) and gameID = :gameID");
		$stmt->bindParam(':gameID', $gameID);
		//set in turnInfo?
		$stmt->bindParam(':turn', $turn);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result['username'];
	}



	public function pullWilds ($turnID, $wilds, $dbh)
	{
		$stmt = $dbh->prepare("update turns set wilds_pulled = (wilds_pulled + :wilds) where turnID = :turnID");
		$stmt->bindParam(':wilds', $wilds);
		$stmt->bindParam(':turnID', $turnID);
		$stmt->execute();
	}



	public function updateGameToTurn($turn, $gameID, $dbh)
	{

		$stmt = $dbh->prepare("update games set turn = :turn where gameID = :gameID");
		$stmt->bindParam(':gameID', $gameID);
		$stmt->bindParam(':turn', $turn);
		$stmt->execute();

	}



	public function updateTurnToCurrentRoll ($quantity, $value, $dbh)
	{
		$stmt = $dbh->prepare("update turns set q_passed = :quantity, v_passed = :value where turnID = :turnID");
		$stmt->bindParam(':quantity', $quantity);
		$stmt->bindParam(':value', $value);
		$stmt->bindParam(':turnID', $this->turnID);
		$stmt->execute();
	}




	public function updateTurnStatus ($status, $turnID, $dbh)
	{
		$stmt = $dbh->prepare("update turns set status = :status where turnID = :turnID");
		$stmt->bindParam(':status', $status);
		$stmt->bindParam(':turnID', $turnID);
		$stmt->execute();
	}




	public function updateTurnToFresh ($turnID, $dbh)
	{
		$stmt = $dbh->prepare("update turns set fresh = 1, wilds_pulled = 0 where turnID = :turnID");
		$stmt->bindParam(':turnID', $turnID);
		$stmt->execute();
	}







}

?>
