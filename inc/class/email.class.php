<?php


class email {
	public $userID;

	public function getEmailAddress ($userID, $dbh)
	{
		$stmt = $dbh->prepare("
		select email from users 
		where userID = :userID LIMIT 1, 1");
		$stmt->bindParam(':userID', $userID);
		// $stmt->bindParam(':turnID', $this->turnID);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//retrieve player information
	public function sendEmail ($userID, $subject, $content, $dbh)
	{
		$email = $this->getEmailAddress($userID, $dbh);

		$to = $email;
		$subject = $subject;

		$message = $content;

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <webmaster@example.com>' . "\r\n";
		$headers .= 'Cc: myboss@example.com' . "\r\n";

		mail($to,$subject,$message,$headers);
		
	}




}

?>
