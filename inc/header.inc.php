<!-- <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'> -->
<!DOCTYPE html>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<!-- <meta http-equiv='Content-Type' content='text/html; charset=utf-8' /> -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Dice...tron</title>

<!-- CSS INCLUDES -->
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/dropdown.css">
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
<!-- <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/> -->
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.4/jquery.mobile-1.4.4.min.css" >

<!-- <link rel="stylesheet" href="css/purple.css" /> -->


<link rel="stylesheet" href="css/purple.css" />
<link rel="stylesheet" href="themes/jquery.mobile.icons.min.css" />
<!-- <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile.structure-1.4.5.min.css" /> -->

<!-- JS INCLUDES -->
<!-- <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script> -->
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://code.jquery.com/mobile/1.4.4/jquery.mobile-1.4.4.min.js"></script>

<!-- <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script> -->
<!-- <script src="js/dropdown.js" ></script> -->
<!-- <script type="text/javascript" src="js/jquery.titlealert.js"></script> -->
</head>

<body>
<!-- <div style='padding:5px; float:right; font-size:14px;  clear:right; display:inline; '>
	<strong style='color:white;'>".$_SESSION['username']."</strong>
</div>

<div style='padding:5px; float:right; font-size:14px; clear:both; color:white;'>
	<a href='logout.php'>Log Out</a>
</div> -->

<?php
// error_reporting(E_ALL);

// echo "<div id='debug'>";
// print_r($_SESSION);
// echo "</div>";

if (!isset($_SESSION['loggedIn']))
{
	?>
	<script>
	window.location = "login.html";
	</script>
	<?php
	exit();
} else {
?>
	<div data-role="header">
		<div data-role="navbar">
			<ul>
				<!-- <li></li> -->
				<li><a href="index.php"><? echo $_SESSION['username']; ?></a></li>
				<li><a href="games.php">Games</a></li>
				<li><a href="logout.php">Logout</a></li>
			</ul>
		</div>
	</div>
<?

}
?>
