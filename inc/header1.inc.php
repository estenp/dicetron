<!-- <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'> -->
<!DOCTYPE html>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<!-- <meta http-equiv='Content-Type' content='text/html; charset=utf-8' /> -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Dice...tron</title>

<!-- CSS INCLUDES -->
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/dropdown.css">
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.4/jquery.mobile-1.4.4.min.css" />


<!-- JS INCLUDES -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>
<script src="http://code.jquery.com/mobile/1.4.4/jquery.mobile-1.4.4.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="js/dropdown.js" ></script>
<script type="text/javascript" src="js/jquery.titlealert.js"></script>
</head>

<body>
<!-- <div style='padding:5px; float:right; font-size:14px;  clear:right; display:inline; '>
	<strong style='color:white;'>".$_SESSION['username']."</strong>
</div>

<div style='padding:5px; float:right; font-size:14px; clear:both; color:white;'>
	<a href='logout.php'>Log Out</a>
</div> -->

<?php


// echo "<div id='debug'>";
// print_r($_SESSION);
// echo "</div>";

if (!isset($_SESSION['loggedIn']))
{
	?>
	<script>
	window.location = "login.html";
	</script>
	<?php
}

	echo "
	<div id='header'>

		<div class='dropdown left' id='mygames'>
			<a class='gamesTab navheader buttonHead' href='#url'>Games</a>
			<div id='gamesSub' class='submenu'>
				<ul class='root'>";
				$i = 1;
				foreach ($getUserGames as $games)
				{
					?>
					<li class='buttonHead n<?php echo $i; ?>' <? if ($games['turn'] == $games['turn_position']){?> style='background-color:#E0CDE2;' <?php } ?> >
						<a href='gamedetail.php?gameID=<?php echo $games['gameID']; ?>'><?php echo $games['name']; ?></a>
					</li>
					<?php
					$i++;
				}
				// echo "<li class='buttonHead n".$i."'><a href='games.php'>Join a Game</a></li>";
				// $i++;
				// echo "<li class='buttonHead n".$i."'><a href='creategame.php'>Create a Game</a></li>";

				echo "</ul>
			</div>
		</div>

		<div class='dropdown right' id='account'>
			<a class='accountTab navheader buttonHead' href='#'>".$_SESSION['username']."</a>
			<div id='accountSub' class='submenu'>
				<ul class='root'>
					<li class='buttonHead'><a href='index.php'>Home</a></li>
					<li class='buttonHead n".$i."'><a href='games.php'>Join a Game</a></li>
					<li class='buttonHead n".$i."'><a href='creategame.php'>Create a Game</a></li>
					<li class='buttonHead'><a href='logout.php'>Log Out</a></li>

				</ul>
			</div>
		</div>




	</div>";

/*<div style='margin-left:auto; margin-right:auto;'>
		<img width='300px' src='dicetron.png'>
	</div>*/
?>
