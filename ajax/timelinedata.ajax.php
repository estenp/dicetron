<?php
include "../inc/config.inc.php";
//get timeline
// print_r($_POST['gameID']);
// exit;
$timeline = new timeline;
$gameID = $_POST['gameID'];
$getTimeline = $timeline->getTimeline($gameID, $dbh);

$player = new player;
$getAllPlayersInfoCount = $player->getAllPlayersInfoCount($gameID, $dbh);
// var_dump($getGameStats);
// exit;
$playerTurnPos = $getGameStats['turn_position'];
$gameTurn = $getGameStats['turn'];

date_default_timezone_set('America/Chicago');
?>


<div id="timeline">
	<ul>
	<?php
		$i = 0;
		foreach ($getTimeline as $value) {
			$date = new datetime($value['add_date']);
			echo '<li><span class="circle ';
			if ($value['code'] == 1){
				echo "green";
			} else if ($value['code'] == 2){
				echo "green";

			} else if ($value['code'] == 3){
				echo "yellow";

			} else if ($value['code'] == 4){
				echo "blue";

			} else if ($value['code'] == 5){
				echo "violet";

			} else if ($value['code'] == 6){
				echo "red";

			} else if ($value['code'] == 7){
				echo "red";

			}
			if($i == 0) {$color = "#E5CA85";} else {$color = "#918FB3";}
			echo '"></span><span style="color:'.$color.'">'.$value['message'].'<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.date_format($date,"D m/d/y g:i a").'</span></li>';
			++$i;
		}
	?>
	</ul>
</div>
