<div id="timeline">
	<ul>
	<?php
		$timeline = new timeline;
		$getTimeline = $timeline->getTimeline($gameID, $dbh);
		foreach ($getTimeline as $value) {
			$date = new datetime($value['add_date']);	
			echo '<li><span class="circle ';
			if ($value['code'] == 1){
				echo "green";
			} else if ($value['code'] == 2){
				echo "green";

			} else if ($value['code'] == 3){
				echo "yellow";
				
			} else if ($value['code'] == 4){
				echo "blue";
				
			} else if ($value['code'] == 5){
				echo "violet";
				
			} else if ($value['code'] == 6){
				echo "red";
				
			} else if ($value['code'] == 7){
				echo "red";
				
			}
			echo '"></span>'.$value['message'].'<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.date_format($date,"D d/m/y g:i a").'</li>';
		}
	?>
	</ul>
</div>