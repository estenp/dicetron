<?php
include "../inc/config.inc.php";
session_start();
error_reporting(E_ALL);
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
error_reporting(-1);
date_default_timezone_set('America/Chicago');

$userID = $_SESSION['userID'];

include_once "../includes.php";
include_once "../inc/queries/gamepageQueries.inc.php";

$_SESSION['playerID'] = $getPlayerInfo['playerID'];

// echo "Playercount: ".$playercount;

//the unique identifier for the current player's turn
$turnID = $turninfo['turnID'];
$currentRoll = $turninfo['q_received'] . " " . $turninfo['v_received'];
$quantPassed = $turninfo['q_received'];
$valuePassed = $turninfo['v_received'];

$prevquantPassed = $prevturninfo['q_received'];
$prevvaluePassed = $prevturninfo['v_received'];

//the identifier for the previous turn
$prevturnID = $prevturninfo['turnID'];

//set passer
// $passer = $getpasser['playerID'];
$passer = $turninfo['playerID'];
$passername = $Turn->getPasserName($gameID, $dbh);
// echo $passername;

//get game status
$gamestatus = $gameinfo['status'];

//identifies whose turn it is for the current game
$turn = $turninfo['turn'];
$_SESSION['turn'] = $turn;
$status = $turninfo['status'];
// print_r($turninfo);
//decide if this roll is off fresh
$freshflag = $turninfo['fresh'];

// SELECT MOST RECENT ROLL
$lastRollArray = $Roll->getLastRoll($gameID, $dbh);
$lastRollQuantArray = $Roll->valueArraytoQuantArray($lastRollArray);


$wildspulled = $turninfo['wilds_pulled'];
$wilds = $lastRollQuantArray[1];
// echo "WILDS: ".$wilds;

//FOR TIMELINE
$stats = new stats;
$player = new player;
$getTimeline = $timeline->getTimeline($gameID, $dbh);
$getGameStats = $stats->getGameStats($gameID, $dbh);
$getAllPlayersInfoCount = $player->getAllPlayersInfoCount($gameID, $dbh);
$playerTurnPos = $getGameStats['turn_position'];
$gameTurn = $getGameStats['turn'];


if (count($lastRollArray) > 0) {
	$bestRoll = $Roll->judgeRoll($lastRollQuantArray);
	// echo "Best Roll: ";
	// print_r($bestRoll);
}
else {
	$bestRoll = array('value' => 2, 'quantity' => 2);
}


if ($turn == $getPlayerInfo['turn_position'])
{
	$myTurn = true;
}
else
{
	$myTurn = false;
}


?>



<div id="theTable" >

	<?PHP

	//BEGIN FORM
	echo "
	<form method='post' data-ajax='false' action='turnprocess.php' id='procform'>
		<input type='hidden' name='turnID' value='".$turnID."' />
		<input type='hidden' name='turn' value='".$turn."' />
		<input type='hidden' name='gameID' value='".$gameID."' />
		<input type='hidden' name='playerID' value='".$_SESSION['playerID']."' />
		<input type='hidden' name='playercount' value='".$playercount."' />
		<input type='hidden' name='wilds' value='".$wilds."' />
		<input type='hidden' name='wildspulled' value='".$wildspulled."' />
		<input type='hidden' name='passer' value='".$passer."' />
		<input type='hidden' name='quantitybest' id='quantity' value='".$bestRoll['quantity']."' />
		<input type='hidden' name='valuebest' id='value' value='".$bestRoll['value']."' />
		<input type='hidden' name='quantpassed' id='quantpassed' value='".$quantPassed."' />
		<input type='hidden' name='valuepassed' id='valuepassed' value='".$valuePassed."' />

		<!--<input type='checkbox' hidden name='proc' id='fresh' value='fresh' >-->
		<input type='checkbox' hidden name='proc' id='pass' value='pass' >
		<input type='checkbox' hidden name='proc' id='look' value='look' >
		<input type='checkbox' hidden name='proc' id='reroll' value='reroll' >
		<input type='checkbox' hidden name='proc' id='pull' value='pull' >";
	?>

		<!-- <div id="rollContainer"> -->
		<?php
		// UNIVERSAL VARIABLES NEEDED TO PROCESS
		// BEGIN FORM AND PASS MAIN VARIABLES

		if ($gamestatus == false){
			echo "<p>Game is not currently active.</p>";
		}
		if ($gamestatus == false AND $playercount < $gameinfo['playermax']) {
			echo "<p>There are ".$playercount." player(s) in the game. You need ".($gameinfo['playermax'] - $playercount)." more to begin.</p>";
		}


		//IF IT IS THE USERS TURN AND STATUS IS FRESH ROLL, DISPLAY ROLL BUTTON, SET STATUS TO fresh
		if ($myTurn == true AND $gamestatus == true)
		{
			// echo $turninfocount."- Turn info count<br />";
			if (/*$turninfocount == 1 and*/ $status == 'fresh')
			{
				echo "
				<div class='center'>
						<input type='submit' value='Roll'>
						<input type='hidden' name='wilds' value='0' />
						<input type='hidden' name='proc' value='fresh' />
				</div>";
			}
			// OTHERWISE, GENERATE ROLL CUP, RE-ROLL BUTTON, PASS BUTTON, LOOK BUTTON, AND ROLL SELECTS
			else
			{
				//SHOW ANY WILDS
				echo "<div id='wildsdiv'>";
				for ($i = 0; $i < $wildspulled; $i++) {
					echo "<div id='dicesprite1'></div>";
				}
				echo "</div>";
				?>

				<!-- </div> -->
				<div id="buttonsContainer">
					<? // ALWAYS INCLUDE PASS BUTTON
					if ($currentRoll !== '5 6' or ($currentRoll == '5 6' and $freshflag == 1)){
						?> <a href='#selectdiv' id='passButton' data-rel="popup" data-position-to="window" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-icon-arrow-r ui-btn-icon-left ui-btn-a" data-transition="pop">Pass</a> <?
					}
					// IF STATUS IS LOOKED, SHOW REROLL BUTTTON
					if ($status == 'looked') {
						?> <a href='#' id='reRollButton' data-rel="popup" data-position-to="window" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-icon-back ui-btn-icon-left ui-btn-a" data-transition="pop">Re-Roll</a> <?
					}

					// IF STATUS IS COVERED, SHOW PULL AND LOOK BUTTONS
					if ($status == 'covered') {
						?> <button id='pullButton'  class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-icon-arrow-u ui-btn-icon-left ui-btn-a" >Pull</button> <?
						if ($currentRoll !== '5 6'){
							?> <button id='lookButton' class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-icon-eye ui-btn-icon-left ui-btn-a"  >Look</button> <?
						}
					}
					?>
				</div>


				<?php// IF STATUS IS LOOKED OR rerolled, SHOW ROLLCUP
				if ($status == 'looked' || $status == 'rerolled' || $status == 'off fresh')
				{
					?>

						<div  class='center'>

							<div id='dice'>
							<?php
							foreach ($lastRollArray as $value) {
								/*echo "[";
									if ($value == 1) {
										echo "<span style='color:#00D10E'>".$value."</span>";
									}
									else {
										echo $value;
									}

								echo "]  ";*/
								echo "<div id='dicesprite".$value."'></div>";
							}?>

							</div>
						</div>
				<?php
				}
				?>



				<div data-role="popup" id="selectdiv" data-theme="a" class="ui-corner-all">
				<!-- <div id='selectdiv' data-role="popup" data-overlay-theme="b" data-theme="b" data-dismissible="false" style="max-width:400px;"> -->
					<fieldset data-role="controlgroup" data-type="horizontal" class='center'>


						<?php // // IF STATUS IS FRESH, OR TURN IS OFF FRESH ROLL, I.E. THERE IS NO CURRENT ROLL TO BEAT, DISPLAY ALL OPTIONS

						if ($status == 'off fresh' or $freshflag == 1) {
							echo
								"<div class='center'>

									<select id='quantity' name='quantity'>
										<option value='2'>two</option>
										<option value='3'>three</option>
										<option value='4'>four</option>
										<option value='5'>five</option>
									</select>

									<select id='value' name='value'>
										<option value='2'>twos</option>
										<option value='3'>threes</option>
										<option value='4'>fours</option>
										<option value='5'>fives</option>
										<option value='6'>sixes</option>
									</select>&nbsp;&nbsp;&nbsp;&nbsp;
								</div>";
						}
						else {
							include "../rollselects.php";
						}
					?></fieldset>
					<input type="button" id="selectRollButton" value="Pass">
				</div><?php
			}
		}
		else {
			?>
			<div class="column">
				<!-- output last timeline event -->
				<?php print_r($getLastTimelineEvent($gameID, $dbh)); ?>

				<!-- output game stats -->
				<table class="center" id="statTable" >
				<tr><b><th>Player</th><th>Turn Position</th><th>Folds Taken</th><th>Folds Given</th></b></tr>
				<?php

				foreach ($getGameStats as $statsdata)
				{
					echo "<tr style='text-align:right;";
					if ($statsdata['folds_taken'] == 4) {
						echo " color:#aaa; font-style:italic;'";
					}
					echo "'><td>".$statsdata['username']."</td><td>".$statsdata['turn_position']."</td><td";
					if ($statsdata['folds_taken'] == 3) {
						echo " style='color:red; font-weight:bold;'";
					}

					echo ">".$statsdata['folds_taken']."</td><td>".$statsdata['folds_given']."</td><td></td></tr>";
				}
				?>
			</table>
		</div> <?php
		}
		?>
	</form>


	<script>
	$(document).ready(function() {
		$('#theTable').enhanceWithin();
		$('#lookButton').click(function (){
			$('#look').prop('checked', true);
			$('#procform').submit();
		});
		$('#selectRollButton').click(function (){
			$('#pass').prop('checked', true);
			$('#procform').append('<input type="hidden" name="quantity" value="'+$("select#quantity").val()+'" />');
			$('#procform').append('<input type="hidden" name="value" value="'+$("select#value").val()+'" />');
			$('#procform').submit();
		// 	selectPass();
		});
		$('#pullButton').click(function (){
			$('#pull').prop('checked', true);
			$('#procform').submit();
		});
		$('#reRollButton').click(function (){
			$('#reroll').prop('checked', true);
			$('#procform').submit();
		});
	});
	</script>
	<script>
	$('#stats').animate({
	    scrollTop: $('#stats').get(0).scrollHeight
	}, 1500);
	</script>
</div>


