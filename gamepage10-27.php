<?php
session_start();

include "includes.php";
include "dbconnect.php";

$gameID = $_GET['gameID'];
$_SESSION['gameID'] = $gameID;

//QUERIES

//gather player information
$myplayerinfosql = "select * from players p
join games g on (p.gameID = g.gameID)
join users u on (u.userID = p.userID)
/*join stats s on (s.gameID = g.gameID and s.playerID = p.playerID)*/
where u.userID = ".$_SESSION['userID']." and g.gameID = ".$gameID;
$myplayerinfo = mysqli_query($con, $myplayerinfosql);
$myplayerinfoResult = mysqli_fetch_array($myplayerinfo);


$_SESSION['playerID'] = $myplayerinfoResult['playerID'];


//gather player count
$playercountsql = "select * from players p
join games g on (g.gameID = p.gameID)
where g.gameID = ".$gameID;
$playercountResult = mysqli_query($con, $playercountsql);
$playercount = mysqli_num_rows($playercountResult);
$_SESSION['playercount'] = $playercount;

echo "Playercount: ".$playercount;




//gather turn count and other turn info
$turninfosql = "select * from turns t
join games g on (t.gameID = g.gameID) where g.gameID = ".$gameID." order by turnID desc";
// echo $turninfosql;
$turninfoResult = mysqli_query($con, $turninfosql);
$turninfocount =  mysqli_num_rows($turninfoResult);
$turninfo = mysqli_fetch_array($turninfoResult);

//the unique identifier for the current player's turn
$turnID = $turninfo['turnID'];
$currentRoll = $turninfo['r_received'];

//the identifier for the previous turn
$prevturnID = $turninfo['turnID'] - 1;

echo "TurnID".$turnID . "<br />";
//echo $prevturnID;
//identifies whos turn it is for the current game
$turn = $turninfo['turn'];
$_SESSION['turn'] = $turn;
echo "Turn: ".$turn;
$status = $turninfo['status'];




// SELECT MOST RECENT ROLL
$fetchRollsql = "select * from rolls where gameID = ".$gameID." order by roll_time desc limit 0, 1";
$fetchRollResult = mysqli_query($con, $fetchRollsql);
$rollArray = Array();
$wilds = 0;
while ($fetchRoll = mysqli_fetch_array($fetchRollResult)) {
	// echo $fetchRoll['fours'];

	for ($i = 0; $i < $fetchRoll['wilds']; $i++) {
		$rollArray[] = 1;
		$wilds = ++$wilds;

	}
	for ($i = 0; $i < $fetchRoll['twos']; $i++) {
		$rollArray[] = 2;
	}
	for ($i = 0; $i < $fetchRoll['threes']; $i++) {
		$rollArray[] = 3;
	}
	for ($i = 0; $i < $fetchRoll['fours']; $i++) {
		$rollArray[] = 4;
	}
	for ($i = 0; $i < $fetchRoll['fives']; $i++) {
		$rollArray[] = 5;
	}
	for ($i = 0; $i < $fetchRoll['sixes']; $i++) {
		$rollArray[] = 6;
	}
}
// exit();
print_r($rollArray);


//if this is the first turn, set session variable to note a fresh roll
//this should be done after each fold as well -- need to implement
//***new turns are not being added**




//if the player's turn position matches the games current turn, set myturn to true
if ($turn == $myplayerinfoResult['turn_position'])
{
	$myTurn = true;
}
else
{
	$myTurn = false;
}

/*if (isset($myTurn)){
	echo $myTurn;
}*/



//START CONTENT

include "header.php";


// PLAYER CHECK
if (isPlayer($_SESSION['userID'], $gameID) == false)
{
	/*echo "<script>
	window.location = 'join.php';
	</script>";*/
	echo "The current user is not in this game.";
	exit();
}
else
{
?>
	<!-- //left column -->

	<!-- INFORMATION HUD PANE -->
	<div id="left-col" >

		<table text-align="right">
		<tr>
			<td><b>Turn:</b></td>
			<td>
			<?php	
				if ($myTurn == true)
					{
						echo "It's your turn";
					}
				else
					{
						echo getTurn($turn, $gameID);
					}
			?>
			</td>
		</tr>
		<tr>
			<td><b>Current Roll:</b></td>
			<td>
				<?php echo $currentRoll ?>
			</td>
		</tr>
		<tr>
			<td><b>Wilds:</b></td>
			<td><?php echo $wilds ?></td>
		</tr>
		<tr>
			<td><b>Previous Roll:</b></td>
			<td></td>
		</tr>
		<tr>
			<td><b>On Deck:</b></td>
			<td>
				<?php
					echo getOnDeck($turn, $gameID);
				?>
			</td>
		</tr>
		</table>

	</div>

	<!-- //middle column -->
	
	<!-- ROLL INFORMATION -->
	<div class="center" id="middle-col">
		<div id="wilds">
			

		<!-- GAME STATS -->
		<table class="center" id="statTable">
		<tr><b><th>Player</th><th>Folds Taken</th><th>Folds Given</th><th>Executions</th><th>Five Sixes</th><th>Special Rolls</th></b></tr>
			<?php
			while ($statsdata = mysqli_fetch_array($myplayerinfo))
			{
				echo "<tr><td>".$statsdata['playerID']."</td><td>".$statsdata['folds_taken']."</td><td>".$statsdata['folds_given']."</td><td>".$statsdata['executions']."</td><td>".$statsdata['five_sixes']."</td><td></td></tr>";
			}
			?>
		</table>


	<?php
	// BEGIN FORM AND PASS MAIN VARIABLES
	echo "<form method='post' action='turnprocess.php'>


			<input type='hidden' name='turnID' value='".$turnID."' />
			<input type='hidden' name='turn' value='".$turn."' />
			<input type='hidden' name='gameID' value='".$gameID."' />
			<input type='hidden' name='playerID' value='".$_SESSION['playerID']."' />";

			
		//IF IT IS THE USERS TURN AND STATUS IS FRESH ROLL, DISPLAY ROLL BUTTON, SET STATUS TO fresh
		if ($myTurn == true) {
			echo $turninfocount."- Turn info count<br />";
			echo $status."- Status";
			if (/*$turninfocount == 1 and*/ $status == 'fresh')

			{
				echo "
				<div class='center'>
						<input type='submit' value='Roll'>
						<input type='hidden' name='wilds' value='0' />
						<input type='hidden' name='proc' value='fresh' />
				</div>";
			}
			// OTHERWISE, GENERATE ROLL CUP, RE-ROLL BUTTON, PASS BUTTON, LOOK BUTTON, AND ROLL SELECTS
			else
			{
				echo "
				<div class='center'>
					<div id='dicecup' style='display:none'>";
					// echo "$rollArray is looking at the current turn.. should look at previous turn unless it's a reroll";
					
					foreach ($rollArray as $value) {
						echo "[";
							if ($value == 1) {
								echo "<span style='color:#00D10E'>".$value."</span>";
							}
							else {
								echo $value;
							}
						
						echo "]  ";
					}
				echo "</div>";
				print_r($rollArray);
					
				// IF THIS IS AFTER A FRESH ROLL, ALLOW FOR REROLL
				if ($status != 'offFresh' and $status != 'rerolled') {
					echo "
							<div id='rerollbtn' class='center' style='display:none'>
									<input type='submit' value='Re-Roll' >
							</div>
									
							<input type='hidden' name='wilds' value=".$wilds." />
							<input type='hidden' name='proc' value='reroll' />
							<input type='hidden' name='playercount' value='".$playercount."' />

						";
						
				}

				// IF STATUS IS FRESH, OR TURN IS OFF FRESH ROLL, I.E. THERE IS NO CURRENT ROLL TO BEAT, DISPLAY ALL OPTIONS
				if ($status == 'fresh' or $status == 'offFresh') {
					echo "<div class='center'>
							<select name='quantity'>
								<option>two</option>
								<option>three</option>
								<option>four</option>
								<option>five</option>
							</select>
							<select name='value'>
								<option>twos</option>
								<option>threes</option>
								<option>fours</option>
								<option>fives</option>
								<option>sixes</option>
							</select>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>

						<input type='hidden' name='playercount' value='".$playercount."' />
						<input type='hidden' name='wilds' value='".$wilds."' />
						<input type='hidden' name='proc' value='pass' />

						<div class='center'>	
								<input type='submit' value='Pass'>
						</div>

					
					<input type='button' id='look' value='Look' >";

				// ELSE THE SELECTS SHOULD ONLY ALLOW YOU TO SELECT OPTIONS THAT ARE HIGHER THAN THE REQUIRED Roll
				} else {
					//loop through both quants and values each iteration if the highest possible is greater than or equal to that index
					//output the option for it
					$count = 0;
					//need to get passed roll and limit options based on that
					$passedRoll = 0;
					
					
					
						echo "<div class='center'>
							<select name='quantity'>
								<option>two</option>
								<option>three</option>
								<option>four</option>
								<option>five</option>
							</select>
							<select name='value'>
								<option>twos</option>
								<option>threes</option>
								<option>fours</option>
								<option>fives</option>
								<option>sixes</option>
							</select>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>

						<input type='hidden' name='playercount' value='".$playercount."' />
						<input type='hidden' name='wilds' value='".$wilds."' />
						<input type='hidden' name='proc' value='pass' />



						
						<div class='center'>	
								<input type='submit' value='Pass'>
						</div>

					
					<input type='button' id='look' value='Look' >";
				}
					/*if ($status == 'rolled' or $status == 'offFresh' or $status == 'unrolled') {
						echo "<input type='button' id='look' value='Look' >";
					}*/
					
				echo "</div>";
			}
		}
	echo "</form>";
	?>

	</div>



	<!-- right column -->
	<div id="right-col" class="opaque">
		<strong>Comments and stuff</strong>
	</div>


	</body>
	</html>

<?php
// END PLAYER CHECK
}



// print_r(sort($asd));
// print_r(judgeRoll());



?>
<script type="text/javascript">
	$('#look').click(function (){
		$('#dicecup, #rerollbtn').show();
		$('#look').hide();
	});
</script>