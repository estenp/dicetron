<?php

include "includes.php";

//roll
$roll = roll(0);
// print_r($roll);

//create an array for each value with their numeric name and set value to 0
$wilds = array(0 => 1, 'value' => 0);
$twos = array(0 => 2, 'value' => 0);
$threes = array(0 => 3, 'value' => 0);
$fours = array(0 => 4, 'value' => 0);
$fives = array(0 => 5, 'value' => 0);
$sixes = array(0 => 6, 'value' => 0);
//create a multidimensional array from these (could be more concise)
$rollArray = array(
	1 => $wilds,
	2 => $twos,
	3 => $threes,
	4 => $fours,
	5 => $fives,
	6 => $sixes
);
// print_r($roll);
// exit();

//sort the roll and place the quantities for each given value in their respected array
foreach ($roll as $value) {


	if ($value == 1) {
		$rollArray[1]['value'] == $rollArray[1]['value']++;
	}
	else if ($value == 2) {
		$rollArray[2]['value'] == $rollArray[2]['value']++;
	}
	else if ($value == 3) {
		$rollArray[3]['value'] == $rollArray[3]['value']++;
	}
	else if ($value == 4) {
		$rollArray[4]['value'] == $rollArray[4]['value']++;
	}
	else if ($value == 5) {
		$rollArray[5]['value'] == $rollArray[5]['value']++;
	}
	else if ($value == 6) {
		$rollArray[6]['value'] == $rollArray[6]['value']++;
	}
	
}

print_r($rollArray);
//set variables for highest die and highest quantity
$maxQuant = 0;
$maxValue = 0;
//determine highest roll possible
//loop through values excluding wilds (1) and set highest value as max
for ($i = 2; $i <= 6; $i++) {
	$value = $rollArray[$i]['value'];
	// echo "<br />".$value."<br />";
	if ($value > $maxQuant) {
		$maxQuant = $value;
		$maxValue = $i;
	}
	else if ($value == $maxQuant) {
		if ($i > $maxValue) {
			$maxValue = $i;
		}
	}
}
//add final max to the quant of wilds to get the highest roll
$maxQuant = $maxQuant + $rollArray[1]['value'];

echo "<br />".$maxQuant."&nbsp;";
echo $maxValue."'s";


//if the quantity is 5, the new roll will have to be at least two of a higher value
//should be taking care of 5 6's situations prior to this point?
if ($maxQuant == 5) {
	$quant = 2;
	$value = ++$maxValue;
}
else {
	$quant = ++$maxQuant;
	$value = $maxValue;
}




echo "<br>player must pass at least: ".$quant." ".$value."'s";

echo "<div class='center'>
		<select name='quantity'>";

for ($i=$quant; $i < 6; $i++) {
	echo "<option>".$i."</option>";
}

echo "  </select>

		<select name='value'>";

for ($i=$value; $i < 7; $i++) {
	echo "<option>".$i."'s</option>";
}

echo "</select>&nbsp;&nbsp;&nbsp;&nbsp;
	</div>";
?>