<?php
include "includes.php";

extract($_POST);

	if (!isset($password)) {
		$password = '';
	}
	//insert into games
	$game = new game;
	$gameID = $game->addGame($name, $password, $players, $dbh);

	//insert into players, user
	$players = new player;
	$playerID = $players->addPlayer($_SESSION['userID'], $gameID, 1, $dbh);

	//make the game creator the game leader
	$game->updateGameLeader ($gameID, $playerID, $dbh);

	//insert into stats
	$stats = new stats;
	$stats = $stats->addStatRecord($playerID, $gameID, $dbh);

	//insert first turn
	$turn = new turn;
	$turnID = $turn->addNextTurn (1, $gameID, 2, 1, 0, 'fresh', 1, $dbh);

	header("Location:index.php");

?>
