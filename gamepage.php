<?php
session_start();
// error_reporting(E_ALL);
$gameID = $_GET['gameID'];
$_SESSION['gameID'] = $gameID;
$userID = $_SESSION['userID'];

include_once "includes.php";
include_once "inc/queries/gamepageQueries.inc.php";


$_SESSION['playerID'] = $getPlayerInfo['playerID'];
$playerID = $_SESSION['playerID'];

// echo "Playercount: ".$playercount;
// echo "active Playercount: ".$getAllPlayersInfoCount;

//the unique identifier for the current player's turn
$turnID = $turninfo['turnID'];
$currentRoll = $turninfo['q_received'] . " " . $turninfo['v_received'];
$quantPassed = $turninfo['q_received'];
$valuePassed = $turninfo['v_received'];

$prevquantPassed = $prevturninfo['q_received'];
$prevvaluePassed = $prevturninfo['v_received'];

//the identifier for the previous turn
$prevturnID = $prevturninfo['turnID'];

//set passer
// $passer = $getpasser['playerID'];
$passer = $turninfo['playerID'];
$passername = $Turn->getPasserName($gameID, $dbh);
// echo $passername;

//get game status
$gamestatus = $gameinfo['status'];

//identifies whose turn it is for the current game
$turn = $turninfo['turn'];
$_SESSION['turn'] = $turn;
$status = $turninfo['status'];
// print_r($turninfo);
//decide if this roll is off fresh
$freshflag = $turninfo['fresh'];

// SELECT MOST RECENT ROLL
$lastRollArray = $Roll->getLastRoll($gameID, $dbh);
$lastRollQuantArray = $Roll->valueArraytoQuantArray($lastRollArray);

// GET PLAYER STATS
$getPlayerStats = $Stats->getPlayerStats($_SESSION['playerID'], $gameID, $dbh);



$wildspulled = $turninfo['wilds_pulled'];
$wilds = $lastRollQuantArray[1];
// echo "WILDS: ".$wilds;

// echo "Last roll: ";
// print_r($lastRollArray);
if (count($lastRollArray) > 0) {
  $bestRoll = $Roll->judgeRoll($lastRollQuantArray);
  // echo "Best Roll: ";
  // print_r($bestRoll);
}
else {
  $bestRoll = array('value' => 2, 'quantity' => 2);
}


//START CONTENT
include "inc/header.inc.php";

echo "<div>";
?>
    <div id="statButtonbar">
      <div id="statButton">
        <div id="folds">
        <span class="foldCircle" id="firstFold"></span>
        <span class="foldCircle" id="secondFold"></span>
        <span class="foldCircle" id="thirdFold"></span>
        <span class="foldCircle" id="fourthFold"></span>
        </div>
        <?php
          $folds = $getPlayerStats['folds_taken'];


          // if ($folds == 0){
          //   echo "No Folds";
          // } else if ($folds == 1) {
          //   echo "One Fold";
          // } else if ($folds == 2) {
          //   echo "Two Folds";
          // } else if ($folds == 3) {
          //   echo "Three Folds";
          // } else if ($folds == 4) {
          //   echo "You out, dog.";
          // }

        ?>
      </div>
    </div>

    <!-- <div id="logodiv">
      <img src="images/logo.png">
    </div> -->

    <?php
    // PLAYER CHECK
    if (!$Player->isPlayer($_SESSION['userID'], $gameID, $dbh))
    {
      echo "<script>
      window.location = 'gamedetail.php?gameID=".$gameID."'
      </script>";
      echo "The current user is not in this game.";
      exit();
    }
    else
    {

      //if the player's turn position matches the games current turn, set myturn to true
      if ($turn == $getPlayerInfo['turn_position'])
      {
        $myTurn = true;
      }
      else
      {
        $myTurn = false;
      }

      // if ($_SESSION['userID'] == 1) {
      //   echo "<form action='turnprocess.php'>
      //       <input type='hidden' name='proc' value='reset' />
      //       <input type='hidden' name='gameID' value='".$gameID."' />
      //       <input type='submit' name='reset' value='Reset Game' />
      //     </form>";
      // }

    ?>

      <!-- //left column -->

      <!-- INFORMATION HUD PANE -->
      <!-- <div id="left-col" > -->

    <!-- <div id="infoTab">
    </div> -->
    <!-- <div id="rollTab">
    </div> -->
    <a href="#timeline"><div id="timelineTab"></div></a>
    <a href="#chat"><div id="chatTab"></div></a>

    <!-- </div> -->

    <!-- //middle column -->
    <?php /*include_once "gamepageMiddle.php";*/ ?>

    <div id="middle-col">
        <div id="theTable">
        </div>


        <div id="timeline" data-role="panel" data-display="overlay" style="overflow:hidden;">
        </div>

        <div id="chat" data-role="panel" data-display="overlay" style="overflow:hidden;">
        	<div id="messenger">
        	  	<div id="chatbox">
        	  		<ul id="chatlist">
        	  		</ul>
        	  	</div>
        	  	<form id="message" name="message" data-ajax="false">
        	      	<input type="text" name="usermsg"  id="usermsg" size="63" placeholder="Type...">
        	      	<br>
        	      	<input type="submit" name="submitmsg"  id="submitmsg" value="Send" >
        	  	</form>
        	</div>
        </div>

    </div>


    <div data-role="footer" style="overflow:hidden;" data-position="fixed">
        <div data-role='navbar' data-position='fixed'>
            <ul>
                <li><a href='gamedetail.php?gameID=<?php echo $gameID; ?>'>Back</a></li>
            </ul>
        </div>
    </div>

<!-- end of 960 container -->
</div>


<?php
// END PLAYER CHECK
}

?>
<script type="text/javascript">

$(document).ready( function() {

    var gameID = <?php echo $gameID ?>;
    var playerID = <?php echo $playerID ?>;

    loadGamepage(gameID);
    loadTimeline(gameID);
    loadChatbox(gameID);

    function loadGamepage (gameID) {
        $('#theTable').load('ajax/theTable.ajax.php', {'gameID': gameID},
        	function( response, status, xhr ) {
				if ( status == "error" ) {
					var msg = "Sorry but there was an error: ";
					alert( msg + xhr.status + " " + xhr.statusText );
				}
			}
		).delay(800).fadeIn("slow");


    }

    function loadTimeline (gameID) {
	    $('#timeline').load('ajax/timelinedata.ajax.php', {'gameID': gameID},
	        function () {
	            $('#timeline').scrollTop($('#timeline').prop("scrollHeight"));
	            // $("#timeline").animate({ scrollTop: $('#timeline')[0].scrollHeight}, 1000);
	            // $('#timeline').animate({
	            //     scrollTop: $('#timeline').scrollHeight
	            // }, 1500);
	        }
	    );
	}

	function loadChatbox (gameID) {
	    $('#chatbox').load('ajax/chatData.ajax.php', {'gameID': gameID, 'playerID':playerID},
	        function () {
	            $("#chatbox").scrollTop($("#chatbox")[0].scrollHeight);
	        }
	    );
	}

    //start ajax interval
    gpInterval = setInterval(function(){
        loadGamepage(gameID);
    }, 30000);




    $('#timeline').on("panelopen", function () {
    	//start ajax interval
    	timelineInterval = setInterval(function(){
    	    loadTimeline(gameID);
    	}, 10000);
    });

    $('#timeline').on("panelclose", function () {
    	//start ajax interval
    	clearInterval(timelineInterval);
    });

    $('#chat').on("panelopen", function () {
    	//start ajax interval
    	chatboxInterval = setInterval(function(){
    	    loadChatbox(gameID);
    	}, 10000);
    });

    $('#chat').on("panelclose", function () {
    	//start ajax interval
    	clearInterval(chatboxInterval);
    });




	$('#message').submit(function(){
		// alert(playerID);
		if ($('#usermsg').val() !== '') {
			$.post("ajax/addChat.ajax.php", {'gameID':gameID, 'playerID':playerID, 'message':$('#usermsg').val()}, function(){
				//load in chat content
				$('#chatbox').load('ajax/chatData.ajax.php', {'gameID': gameID, 'playerID':playerID}, function() {
					$('#usermsg').val('');
					$("#chatbox").scrollTop($("#chatbox")[0].scrollHeight);
				});
				// return false;
			});
		}

		return false;
	});

	// $('#message').submit(function (){
	// 	return false;
	// });



    // //If user submits the form CHAT STUFF
    // $("#message").submit(function(){
    //     var clientmsg = $("#usermsg").val();
    //     $.post("messageprocess.php", {text: clientmsg});
    //     $("#usermsg").prop("value", "");
    //     return false;
    // });


	//FILL FOLD METER
    var folds = <?php echo $folds; ?>;
    for (var i = 0; i <= folds; i++) {
    if (i == 1) {
        $('#firstFold').css('background-color', '#70D294');
    }
    else if (i == 2) {
        $('#secondFold').css('background-color', '#D2CC70');
    }
    else if (i == 3) {
        $('#thirdFold').css('background-color', '#D29870');
    }
    else if (i == 4) {
        $('#fourthFold').css('background-color', '#D27570');
    }
    };

});



</script>
