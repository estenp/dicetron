<?php
session_start();

//checks if the user is already in this game
function isPlayer ($userID, $gameID)
{
	include "dbconnect.php";

	$sql = 
	"select * from games g 
join players p on (p.gameID = g.gameID)
where p.userID = ".$userID." and g.gameID = ".$gameID;

	

	$x = mysqli_query($con, $sql);
	$result = mysqli_fetch_array($x);
	
	
	if ($result != null)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*function isTurn ($playerID, $gameID)
{
	include "dbconnect.php";
	$sql = "select * from games where gameID = ".$gameID." and playerID = ".$playerID;
}*/

//gets the username for the player who's turn it is what?
function getTurn ($turn, $gameID)
{
	include "dbconnect.php";
	$sql = "select username from users u
join players p on (p.userID = u.userID)
where p.turn_position = ".$turn." and gameID = ".$gameID;
	$turnNamequery = mysqli_query($con, $sql);
	$value = mysqli_fetch_array($turnNamequery);
	return $value['username'];
}


//gets the username for the player who's on deck NEEDS TO BE ABLE TO LOOP BACK TO 1 AT THE END OF PLAYERS LIST
function getOnDeck ($turn, $gameID)
{
	include "dbconnect.php";
	$sql = "select username from users u
	join players p on (p.userID = u.userID)
	where p.turn_position = (".$turn." + 1) and gameID = ".$gameID;
	$onDeckquery = mysqli_query($con, $sql);
	$value = mysqli_fetch_array($onDeckquery);
	return $value['username'];
}

function getPasser ($passer, $gameID)
{
	include "dbconnect.php";
	$sql = "select username from users u
	join players p on (p.userID = u.userID)
	where p.playerID = ".$passer." and gameID = ".$gameID;
	$onDeckquery = mysqli_query($con, $sql);
	$value = mysqli_fetch_array($onDeckquery);
	return $value['username'];
}


function getPlayerData ($userID, $gameID)
{
	include "dbconnect.php";
	$sql = "
select * from players p
join games g on (p.gameID = g.gameID)
join users u on (u.userID = p.userID)
where p.userID = ".$userID." and p.gameID = ".$gameID;


	$query = mysqli_query($con, $sql);
	$result = mysqli_fetch_array($query);
	return $result['username'];
}

function roll ($wilds)
{
	$quantity = 5 - $wilds;
	$rollArray = array();
	for ($i = 1; $i <= $quantity; $i++)
	{
		$rollArray[] = rand(1, 6);
	}

	return $rollArray;
	include "dbconnect.php";

}


function judgeRoll ($roll) {
	//find the max quantity from the rolls that isn't wild
		//that is best roll
		//what if there is four wilds and one five?

		$oldWilds = $_POST['wilds'];
		// DEFINES A NEW ROLL
		// $roll = roll($oldWilds);
		// $roll = roll(0);
		
		//create an array for each value with their numeric name and set value to 0
		$wilds = array(0 => 1, 'value' => 0);
		$twos = array(0 => 2, 'value' => 0);
		$threes = array(0 => 3, 'value' => 0);
		$fours = array(0 => 4, 'value' => 0);
		$fives = array(0 => 5, 'value' => 0);
		$sixes = array(0 => 6, 'value' => 0);
		//create a multidimensional array from these (could be more concise)
		$rollArray = array(
			1 => $wilds,
			2 => $twos,
			3 => $threes,
			4 => $fours,
			5 => $fives,
			6 => $sixes
		);

		//sort the roll and place the quantities for each given value in their respected array
		foreach ($roll as $value) {


			if ($value == 1) {
				$rollArray[1]['value'] == $rollArray[1]['value']++;
			}
			else if ($value == 2) {
				$rollArray[2]['value'] == $rollArray[2]['value']++;
			}
			else if ($value == 3) {
				$rollArray[3]['value'] == $rollArray[3]['value']++;
			}
			else if ($value == 4) {
				$rollArray[4]['value'] == $rollArray[4]['value']++;
			}
			else if ($value == 5) {
				$rollArray[5]['value'] == $rollArray[5]['value']++;
			}
			else if ($value == 6) {
				$rollArray[6]['value'] == $rollArray[6]['value']++;
			}
			
		}


		//set variables for highest die and highest quantity
		$maxQuant = 0;
		$maxValue = 0;
		//determine highest roll possible
		//loop through values excluding wilds (1) and set highest value as max
		for ($i = 2; $i <= 6; $i++) {
			$value = $rollArray[$i]['value'];
			// echo "<br />".$value."<br />";
			if ($value > $maxQuant) {
				$maxQuant = $value;
				$maxValue = $i;
			}
			else if ($value == $maxQuant) {
				if ($i > $maxValue) {
					$maxValue = $i;
				}
			}
		}
		//add final max to the quant of wilds to get the highest roll
		$maxQuant = $maxQuant + $rollArray[1]['value'];
		$roll = array('value'=>$maxValue, 'quantity'=>$maxQuant);

		//set roll ranks
		
	return $roll;

}


function incrementTurn ($turn, $game, $playercount) {
		$turn = $turn + 1;

		//if the incremented turn is greater than the number of people playing, go back to 1
		if ($turn > $playercount) {

			$turn = 1;
		}

		//if this status is inactive, continue to increment the turn until it lands on an active player
		$i = 0;
		while ($i == 0) {
			// $turn = $turn + 1;
			$sql = "select status from players where turn_position = ".$turn." and gameID = ".$gameID;
			$execute = mysqli_query($con, $sql);
			$result = mysqli_fetch_array($execute);
			//if the player on the current turn is out of the game, add 1 to turn to go to the next player
			if ($result['status'] == 1) {
				echo "Iteration: ".$i." This player (turn order = ".$turn.") is out of the game..";
				
				$turn = $turn + 1;
				if ($turn > $playercount) {

					$turn = 1;
				}
				$i = 0;
				exit();
			} 
			else {

				$i = 1;
				echo "Player is active...";
			}
		}
	return $turn;
}



/*

//this function would limit turn order to the order in which users joined the game.
function nextTurn (turn) {
if ((turn + 1) > //MAX(MYSQL_INSERT_ID()) ~~ SOMETHING THAT RETRIEVES HIGHEST VALUE IN TURNid FIELD)
{
turn = 1;
}
else
{
turn = turn + 1;
}


function checkTurn (turnID) {

*/


function dbquery ($sql, $return) {
	include "dbconnect.php";
	$query = mysqli_query($con, $sql);

	if ($return == 'array') {
		$result = mysqli_fetch_array($query);
	}
	else if ($return == 'count') {
		$result = mysqli_num_rows($query);
	}
	return $result;
}




?>