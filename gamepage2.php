<?php
session_start();

include "includes.php";
include "dbconnect.php";

$gameID = $_GET['gameID'];
$_SESSION['gameID'] = $gameID;

//QUERIES

//gather player information
$myplayerinfosql = "select * from players p
join games g on (p.gameID = g.gameID)
join users u on (u.userID = p.userID)
/*join stats s on (s.gameID = g.gameID and s.playerID = p.playerID)*/
where u.userID = ".$_SESSION['userID']." and g.gameID = ".$gameID;
$myplayerinfo = mysqli_query($con, $myplayerinfosql);
$myplayerinfoResult = mysqli_fetch_array($myplayerinfo);


$_SESSION['playerID'] = $myplayerinfoResult['playerID'];


//gather player count
$playercountsql = "select * from players p
join games g on (g.gameID = p.gameID)
where g.gameID = ".$gameID;
$playercountResult = mysqli_query($con, $playercountsql);
$playercount = mysqli_num_rows($playercountResult);
$_SESSION['playercount'] = $playercount;




//gather turn count and other turn info
$turninfosql = "select * from turns t
join games g on (t.gameID = g.gameID) where g.gameID = ".$gameID;
$turninfoResult = mysqli_query($con, $turninfosql);
$turninfocount =  mysqli_num_rows($turninfoResult);
$turninfo = mysqli_fetch_array($turninfoResult);

//the unique identifier for the current player's turn
$turnID = $turninfo['turnID'];

//identifies whos turn it is for the current game
$turn = $turninfo['turn'];
$_SESSION['turn'] = $turn;

//if this is the first turn, set session variable to note a fresh roll
//this should be done after each fold as well -- need to implement
if ($turninfocount = 1 and $_SESSION['status'] != 'rolled')
{
	$_SESSION['freshRoll'] = true;
}


//if the player's turn position matches the games current turn, set myturn to true
if ($turn == $myplayerinfoResult['turn_position'])
{
	$myTurn = true;
}
else
{
	$myTurn = false;
}

if (isset($myTurn)){
	echo "My turn: ".$myTurn;
}



//START CONTENT

include "header.php";



if (isPlayer($_SESSION['userID'], $gameID) == false)
{
	/*echo "<script>
	window.location = 'join.php';
	</script>";*/
	echo "wtf?";
}
else
{
?>
	<!-- //left column -->
	<div id="left-col">

		<table text-align:right>
		<tr>
			<td><b>Turn:</b></td>
			<td>
			<?php	
				if ($myTurn == true)
					{
						echo "It's your turn";
					}
				else
					{
						echo getTurn($turn, $gameID);
					}
			?>
			</td>
		</tr>
		<tr>
			<td><b>Current Roll:</b></td>
			<td></td>
		</tr>
		<tr>
			<td><b>Wilds:</b></td>
			<td></td>
		</tr>
		<tr>
			<td><b>Previous Roll:</b></td>
			<td></td>
		</tr>
		<tr>
			<td><b>On Deck:</b></td>
			<td>
				<?php
					echo getOnDeck($turn, $gameID);
				?>
			</td>
		</tr>
		</table>

	</div>

	<!-- //middle column -->
	<div class="center" id="middle-col">


		<table class="center" id="statTable">
		<tr><b><th>Player</th><th>Folds Taken</th><th>Folds Given</th><th>Executions</th><th>Five Sixes</th><th>Special Rolls</th></b></tr>
			<?php
			while ($statsdata = mysqli_fetch_array($myplayerinfo))
			{
				echo "<tr><td>".$statsdata['playerID']."</td><td>".$statsdata['folds_taken']."</td><td>".$statsdata['folds_given']."</td><td>".$statsdata['executions']."</td><td>".$statsdata['five_sixes']."</td><td></td></tr>";
			}
			?>
		</table>


<!-- //decide if this is a fresh roll -->
		<?php

		if ($_SESSION['freshRoll'] == true)
		{
			echo "
			<div class='right'>
				<form method='post' action='roller.php?proc=roll&gameID=".$gameID."&turnID=".$turnID."&playerID=".$_SESSION['playerID']."'>
					<input type='submit' value='Roll'>
					<input type='hidden' name='turnID' value='".$turnID."' />
					<input type='hidden' name='turn' value='".$turn."' />
					<input type='hidden' name='gameID' value='".$gameID."' />
				</form>
			</div>";
		}
		else
		{
		echo "
		<div class='center'>
			<div>";
			if (isset($_SESSION['roll'])) {
				foreach ($_SESSION['roll'] as $value) {
					echo "[".$value."]  ";
				}
			}
		echo "
			</div>
			<form method='post' action='roller.php?proc=pass&gameID=".$gameID."'>
				<div class='left'>
					<select name='quantity'>
						<option>two</option>
						<option>three</option>
						<option>four</option>
						<option>five</option>
					</select>
					<select name='value'>
						<option>twos</option>
						<option>threes</option>
						<option>fours</option>
						<option>fives</option>
						<option>sixes</option>
					</select>&nbsp;&nbsp;&nbsp;&nbsp;
				</div>

				<input type='hidden' name='turnID' value='".$turnID."' />
				<input type='hidden' name='turn' value='".$turn."' />
				<input type='hidden' name='gameID' value='".$gameID."' />
				
				<div class='left'>	
						<input type='submit' value='Pass'>
				</div>
			</form>";

			if ($_SESSION['status'] != 'rolled') {
				
				echo "<form method='post' action='roller.php?proc=reroll&gameID=".$gameID."&turnID=".$turnID."&playerID=".$_SESSION['playerID']."' >
					<div class='right'>
							<input type='submit' value='Re-Roll' onclick='roll(0)'>
					</div>
				</form>";
			}
		echo "</div>";
		}






		?>

	</div>



	<!-- right column -->
	<div id="right-col">
		<strong>Comments and stuff</strong>
	</div>


	</body>
	</html>

<?php
}
?>
