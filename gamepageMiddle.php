<!-- ROLL INFORMATION -->
<div id="middle-col">
	<div id="viewportOuter">
	<div id="viewportInner">

	
		

	<!-- GAME STATS -->
	<table class="center" id="statTable" >
	<tr><b><th>Player</th><th>Turn Position</th><th>Folds Taken</th><th>Folds Given</th><th>Executions</th><th>Five Sixes</th><th>Special Rolls</th></b></tr>
		<?php
		
		foreach ($getGameStats as $statsdata)
		{
			echo "<tr style='text-align:right;";
			if ($statsdata['folds_taken'] == 4) {
				echo " color:#aaa; font-style:italic;'";
			}
			echo "'><td>".$statsdata['username']."</td><td>".$statsdata['turn_position']."</td><td";
			if ($statsdata['folds_taken'] == 3) {
				echo " style='color:red; font-weight:bold;'";
			} 
			
			echo ">".$statsdata['folds_taken']."</td><td>".$statsdata['folds_given']."</td><td>".$statsdata['executions']."</td><td>".$statsdata['five_sixes']."</td><td></td></tr>";
		}
		?>
	</table>

<?php	
	// UNIVERSAL VARIABLES NEEDED TO PROCESS
	echo "
		<form method='post' action='turnprocess.php' id='procform'>
			<input type='hidden' name='turnID' value='".$turnID."' />
			<input type='hidden' name='turn' value='".$turn."' />
			<input type='hidden' name='gameID' value='".$gameID."' />
			<input type='hidden' name='playerID' value='".$_SESSION['playerID']."' />
			<input type='hidden' name='playercount' value='".$playercount."' />
			<input type='hidden' name='wilds' value='".$wilds."' />
			<input type='hidden' name='wildspulled' value='".$wildspulled."' />
			<input type='hidden' name='passer' value='".$passer."' />
			<input type='hidden' name='quantitybest' id='quantity' value='".$bestRoll['quantity']."' />
			<input type='hidden' name='valuebest' id='value' value='".$bestRoll['value']."' />
			<input type='hidden' name='quantpassed' id='quantpassed' value='".$quantPassed."' />
			<input type='hidden' name='valuepassed' id='valuepassed' value='".$valuePassed."' />

			<!--<input type='checkbox' hidden name='proc' id='fresh' value='fresh' >-->
			<input type='checkbox' hidden name='proc' id='pass' value='pass' >
			<input type='checkbox' hidden name='proc' id='look' value='look' >
			<input type='checkbox' hidden name='proc' id='reroll' value='reroll' >
			<input type='checkbox' hidden name='proc' id='pull' value='pull' >
";

// BEGIN FORM AND PASS MAIN VARIABLES

	if ($gamestatus == false){
		echo "<p>Game is not currently active.</p>";
	}
	if ($gamestatus == false AND $playercount < $gameinfo['playermax']) {
		echo "<p>There are ".$playercount." player(s) in the game. You need ".($gameinfo['playermax'] - $playercount)." more to begin.</p>";
	}


	//IF IT IS THE USERS TURN AND STATUS IS FRESH ROLL, DISPLAY ROLL BUTTON, SET STATUS TO fresh
	if ($myTurn == true AND $gamestatus == true) {
		// echo $turninfocount."- Turn info count<br />";
		if (/*$turninfocount == 1 and*/ $status == 'fresh')

		{
			echo "			
			<div class='center'>
					<input type='submit' value='Roll'>
					<input type='hidden' name='wilds' value='0' />
					<input type='hidden' name='proc' value='fresh' />
			</div>";
		}
		// OTHERWISE, GENERATE ROLL CUP, RE-ROLL BUTTON, PASS BUTTON, LOOK BUTTON, AND ROLL SELECTS
		else
		{
			// IF STATUS IS LOOKED OR rerolled, SHOW ROLLCUP
			if ($status == 'looked' || $status == 'rerolled' || $status == 'off fresh'){
				echo "
				<div class='center'>
					<div id='dicecup'>
						<div id='dice'>";
					// echo "$rollArray is looking at the current turn.. should look at previous turn unless it's a reroll";
					
					foreach ($lastRollArray as $value) {
						/*echo "[";
							if ($value == 1) {
								echo "<span style='color:#00D10E'>".$value."</span>";
							}
							else {
								echo $value;
							}
						
						echo "]  ";*/
						echo "<div id='dicesprite".$value."'></div>";
					}
				echo "</div></div>";
			}
			// print_r($rollArray);

			// IF STATUS IS FRESH, OR TURN IS OFF FRESH ROLL, I.E. THERE IS NO CURRENT ROLL TO BEAT, DISPLAY ALL OPTIONS	
			if ($status == 'off fresh' or $freshflag == 1) { 
				echo 
					"<div class='center'>
						<select name='quantity'>
							<option value='2'>two</option>
							<option value='3'>three</option>
							<option value='4'>four</option>
							<option value='5'>five</option>
						</select>
						<select name='value'>
							<option value='2'>twos</option>
							<option value='3'>threes</option>
							<option value='4'>fours</option>
							<option value='5'>fives</option>
							<option value='6'>sixes</option>
						</select>&nbsp;&nbsp;&nbsp;&nbsp;
					</div>";
			}
			else {
				include "rollselects.php";
			}

			// ALWAYS INCLUDE PASS BUTTON
			if ($currentRoll !== '5 6' or ($currentRoll == '5 6' and $freshflag == 1)){
				echo "<input type='button' id='passButton' class='passButton' >";			
			}
			// IF STATUS IS LOOKED, SHOW REROLL BUTTTON
			if ($status == 'looked') {
				echo "<input type='button' id='reRollButton' class='reRollButton'>";
			}

			// IF STATUS IS COVERED, SHOW PULL AND LOOK BUTTONS
			if ($status == 'covered') {
				echo "<input type='button' class='pullButton' id='pullButton' />";
				if ($currentRoll !== '5 6'){
					echo "<input type='button' class='lookButton' id='lookButton'  >";
				}
			}

				
			echo "</div>
				</form>";
		}
	}

?>
	</div>
	</div>

</div>
