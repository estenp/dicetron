<?php
session_start();
$userID = $_SESSION['userID'];
include "includes.php";
// include "dbconnect.php";

include "inc/header.inc.php";

?>
<a class="ui-btn ui-btn-inlineui-icon-plus" href='creategame.php'>Create a Game</a>

<div id="passdiv" style="display:none;">
  <form>
    <p>This game requires a password.</p>
    <input type="text" name="gamepass" id="gamepass" />
    <input type="button" name="join" value="Join" />
  </form>
</div>

<div id="middle-col">
    <!-- <h3>My Games</h3> -->
    <ul class="gameList" data-role="listview"  data-inset="true">
        <li data-role="list-divider">My Games</li>
    <?
    foreach ($getUserGames as $games)
    {
        ?>
        <li <? if ($games['turn'] == $games['turn_position']){ echo 'data-icon="star"'; } else { echo 'data-icon="false"'; } ?> >
            <a href='gamedetail.php?gameID=<?php echo $games["gameID"]; ?>' ><?php echo $games['name']; ?></a>
        </li>
        <?php
        $i++;
    }
    ?>
    </ul>

    <ul class="gameList" data-role="listview" data-inset="true">
        <li data-role="list-divider" >Open Games</li>
        <?php
        $game = new game;
        // print_r($getNonUserGames);
        // exit;
        foreach ($getNonUserGames as $games)
        {
            $playercount = $game->getPlayerCount($games['gameID'], $dbh);

            // if (($games['playermax'] - $playercount) > 0) {
            ?>
            <li <?if (isset($games['password']) && $games['password'] != '') {?> data-icon="lock" <?} else {?> data-icon="false" <?}?>
            >
            <?
                echo "<a href='gamedetail.php?gameID=".$games['gameID']."'>"
                .$games['name']."<span style='font-weight: normal;'><br><br> Player Capacity: ".$games['playermax']." <br>Spots Available: ".($games['playermax'] - $playercount)."</span>
                </a>
            </li>";

        }
        ?>
</div>




<?php
include_once "inc/footer.inc.php";
?>
