<?php
session_start();
$userID = $_SESSION['userID'];
include "includes.php";
// include "dbconnect.php";

include "inc/header.inc.php";

?>
<h3 class="ui-bar ui-bar-a ui-corner-all">Create an Exibition Game</h3>
<div>
    <form action="creategameprocess.php" data-ajax="false" method="post">
        <ul data-role="listview" data-inset="true">
            <li class="ui-field-contain"><input placeholder="Game Name" required type="text" id="name" name="name" ></li>
            <li class="ui-field-contain"><input placeholder="Password (optional)" autocomplete="off" type="password" id="password" name="password" ></li>
            <li class="ui-field-contain">
                <label for="players">Number of Players:</label>
                <input id="players" name="players" value="2" min="2" max="20" data-highlight="true" type="range" >
            </li>
        </ul>

        <input type="submit" value="Create Game">

    </form>
</div>

</body>
</html>
